package com.ble;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.os.Build;
import android.os.Message;
import android.util.Log;

import com.ble.BLeService.TalkInterface;
import com.until.Util;

/**
 * 
 * @author Administrator
 * @since 2013.12.12
 * @version 1.0
 * @version 1.1/2014.8.6
 *          <p>
 *          Add connect to device successfully once to check using gatt.connect
 *          or not
 */
@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
public class BluetoothLeServer implements TalkInterface {

	private static BluetoothLeServer mBluetoothLeService;
	public static final int STATE_BLE_ENABLE_WRITE = 0x33; // 可读写数据状态
	public static final int STATE_BLE_CONNECTED = 0x32; // 已连接
	public static final int STATE_BLE_CONNECTING = 0x31; // 连接中
	public static final int STATE_BLE_DISCONNECTED = 0x30; // 未连接
	private final static String TAG = "blueServer";
	private final static boolean D = true;

	protected Context context;
	protected BluetoothAdapter mBluetoothAdapter;
	protected String mBluetoothDeviceAddress;
	protected BluetoothGatt mBluetoothGatt;
	protected BluetoothManager mBluetoothManager;
	protected int mConnectionState = STATE_BLE_DISCONNECTED;
	/** Read and write object. */
	BluetoothGattCharacteristic mNotifyCharacteristicREAD;
	BluetoothGattCharacteristic mNotifyCharacteristicWRITE;
	// /
	private final List<Message> communicates = new ArrayList<Message>();
	private MessageInterface messageInterface;
	private boolean sucessOnce = false;

	{
		setMessageInterface(new MessageInterface() {
			@Override
			public void addMessagae() {
			}

			@Override
			public void removeMessagae() {
			}
		});
	}

	public interface MessageInterface {
		void addMessagae();

		void removeMessagae();
	}

	protected BluetoothLeServer(Context context) {
		Util.sLog(TAG, "BluetoothLeServer create", "e", true);
		this.context = context.getApplicationContext();
		initialize();
	}

	public static BluetoothLeServer getInstance(Context context) {
		if (mBluetoothLeService == null) {
			mBluetoothLeService = new BluetoothLeServer(context);
		}
		return mBluetoothLeService;
	}

	/**
	 * Initializes a reference to the local Bluetooth adapter.
	 * 
	 * @return Return true if the initialization is successful.
	 */
	public boolean initialize() {
		Util.sLog(TAG, "BluetoothLeServer initialize", "e", true);
		// For API level 18 and above, get a reference to BluetoothAdapter
		// through BluetoothManager.
		if (mBluetoothManager == null) {
			mBluetoothManager = (BluetoothManager) context
					.getSystemService(Context.BLUETOOTH_SERVICE);
			if (mBluetoothManager == null) {
				Log.e(TAG, "Unable to initialize BluetoothManager.");
				return false;
			}
		}
		mBluetoothAdapter = mBluetoothManager.getAdapter();
		if (mBluetoothAdapter == null) {
			Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
			return false;
		}
		return true;
	}

	/**
	 * After using a given BLE device, the app must call this method to ensure
	 * resources are released properly.
	 */
	public void close() {
		if (mBluetoothGatt == null) {
			return;
		}
		clearMessages();
		disconnect();
		mBluetoothGatt.close();
		mBluetoothGatt = null;
	}

	/**
	 * 开始搜索设备
	 */
	public void startLEScanDevice(BluetoothAdapter.LeScanCallback callback) {
		mBluetoothAdapter.startLeScan(callback);
	}

	/**
	 * 停止搜索设备
	 */
	public void stopLEScanDevice(BluetoothAdapter.LeScanCallback callback) {
		mBluetoothAdapter.stopLeScan(callback);
	}

	/**
	 * Connects to the GATT server hosted on the Bluetooth LE device.
	 * 
	 * @param address
	 *            The device address of the destination device.
	 * 
	 * @return Return true if the connection is initiated successfully. The
	 *         connection result is reported asynchronously through the
	 *         {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
	 *         callback.
	 */
	public boolean connect(final String address,
			BluetoothGattCallback mGattCallback) {
		if (mBluetoothAdapter == null || address == null) {
			Log.e(TAG,
					"BluetoothAdapter not initialized or unspecified address.");
			return false;
		}
		if (!address.equals(mBluetoothDeviceAddress))
			setSucessOnce(false);
		// Previously connected device. Try to reconnect.
		if (isSucessOnce() && mBluetoothGatt != null) {
			Log.e(TAG,
					"Trying to use an existing mBluetoothGatt for connection.");
			if (mBluetoothGatt.connect()) {
				mConnectionState = STATE_BLE_CONNECTING;
				return true;
			}
		}
		final BluetoothDevice device = mBluetoothAdapter
				.getRemoteDevice(address);
		if (device == null) {
			Log.w(TAG, "Device not found.  Unable to connect.");
			return false;
		}
		// We want to directly connect to the device, so we are setting the
		// autoConnect
		// parameter to false.
		mBluetoothGatt = device.connectGatt(context, false, mGattCallback);
		Log.e(TAG, "Trying to create a new connection.");
		mBluetoothDeviceAddress = address;
		mConnectionState = STATE_BLE_CONNECTING;
		return true;
	}

	/**
	 * Disconnects an existing connection or cancel a pending connection. The
	 * disconnection result is reported asynchronously through the
	 * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
	 * callback.
	 */
	public void disconnect() {
		mConnectionState = STATE_BLE_DISCONNECTED;

		if (mBluetoothAdapter == null || mBluetoothGatt == null) {
			Log.e(TAG, "BluetoothAdapter not initialized:disconnect");
			return;
		}
		mBluetoothGatt.disconnect();
	}

	public BluetoothGatt getBluetoothGatt() {
		return mBluetoothGatt;
	}

	public BluetoothGattCharacteristic getCharacteristicREAD() {
		return this.mNotifyCharacteristicREAD;
	}

	public BluetoothGattCharacteristic getCharacteristicWRITE() {
		return this.mNotifyCharacteristicWRITE;
	}

	/**
	 * 获取连接状态
	 * 
	 * @return
	 */
	public int getConnectionState() {
		return mConnectionState;
	}

	/**
	 * 获取蓝牙设备的mac地址
	 * 
	 * @return
	 */
	public String geDeviceAddress() {
		return mBluetoothDeviceAddress;
	}

	/**
	 * Retrieves a list of supported GATT services on the connected device. This
	 * should be invoked only after {@code BluetoothGatt#discoverServices()}
	 * completes successfully.
	 * 
	 * @return A {@code List} of supported services.
	 */
	public List<BluetoothGattService> getSupportedGattServices() {
		if (mBluetoothGatt == null)
			return null;
		return mBluetoothGatt.getServices();
	}

	/**
	 * Request a read on a given {@code BluetoothGattCharacteristic}. The read
	 * result is reported asynchronously through the
	 * {@code BluetoothGattCallback#onCharacteristicRead(android.bluetooth.BluetoothGatt, android.bluetooth.BluetoothGattCharacteristic, int)}
	 * callback.
	 * 
	 * @param characteristic
	 *            The characteristic to read from.
	 */
	public void readCharacteristic(BluetoothGattCharacteristic characteristic) {
		if (mBluetoothAdapter == null || mBluetoothGatt == null) {
			Log.e(TAG, "BluetoothAdapter not initialized:readCharacteristic");
			return;
		}
		mBluetoothGatt.readCharacteristic(characteristic);
	}

	public byte[] read() {
		readCharacteristic(mNotifyCharacteristicREAD);
		return mNotifyCharacteristicREAD.getValue();
	}

	/**
	 * Enables or disables notification on a give characteristic.
	 * 
	 * @param characteristic
	 *            Characteristic to act on.
	 * @param enabled
	 *            If true, enable notification. False otherwise.
	 * @param configUUID
	 *            configUUID the UUID of descriptor to set notification.
	 */
	public void setCharacteristicNotification(
			BluetoothGattCharacteristic characteristic, boolean enabled,
			String configUUID) {
		if (mBluetoothAdapter == null || mBluetoothGatt == null) {
			Log.w(TAG,
					"BluetoothAdapter not initialized:setCharacteristicNotification");
			return;
		}
		mBluetoothGatt.setCharacteristicNotification(characteristic, enabled);
		try {
			BluetoothGattDescriptor descriptor = characteristic
					.getDescriptor(UUID.fromString(configUUID));
			if (descriptor != null) {
				Log.e(TAG,
						"set descriptor" + descriptor + descriptor.getValue());
				descriptor
						.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
				mBluetoothGatt.writeDescriptor(descriptor);
			}
		} catch (NullPointerException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
	}

	public void setCharacteristicREAD(
			BluetoothGattCharacteristic characteristicREAD) {
		this.mNotifyCharacteristicREAD = characteristicREAD;
	}

	public void setCharacteristicWRITE(
			BluetoothGattCharacteristic characteristicWRITE) {
		this.mNotifyCharacteristicWRITE = characteristicWRITE;
	}

	public void setConnectionState(int mConnectionState) {
		this.mConnectionState = mConnectionState;
	}

	public void write(byte[] data) {
		if (mNotifyCharacteristicWRITE == null) {
			return;
		}
		Util.show(data, 0, 4, "ble.write");
		addMessage(data);
	}

	public void write(Message message) {
		Util.sLog(TAG, "quest code:" + message.what, "e", D);
		mNotifyCharacteristicWRITE.setValue((byte[]) message.obj);
		writeCharacteristic(mNotifyCharacteristicWRITE);
	}

	protected void writeCharacteristic(
			BluetoothGattCharacteristic characteristic) {
		if (mBluetoothAdapter == null || mBluetoothGatt == null) {
			Log.e(TAG, "BluetoothAdapter not initialized:writeCharacteristic");
			return;
		}
		mBluetoothGatt.writeCharacteristic(characteristic);
	}

	/** Add message */
	public void addMessage(byte[] data) {
		Message message = new Message();
		message.what = BleCdsPax.instance(context)
				.getReturnCode(0xff & data[0]);
		message.obj = data;
		getCommunicates().add(message);
		getMessageInterface().addMessagae();
	}

	/**
	 * 返回BluetoothAdapter
	 * 
	 * @return
	 */
	public BluetoothAdapter getBuleBluetoothAdapter() {
		return mBluetoothAdapter;
	}

	/**
	 * 蓝牙关闭时，设为null
	 */
	public static void setNullForBluetoothLeServer() {
		Util.sLog(TAG, "BluetoothLeServer null", "e", true);
		mBluetoothLeService = null;
	}

	// /for communicate
	@Override
	public void onCharacteristicChanged(
			BluetoothGattCharacteristic characteristic) {
		byte[] value = characteristic.getValue();
		try {
			int code = value[0] & 0xff;
			Util.sLog(TAG, "returned code: 0x" + Integer.toHexString(code),
					"e", D);
			if (getCommunicates().size() > 0) {
				if (code != getCommunicates().get(0).what) {
					if (BleCdsPax.instance(context).isAuto(code))
						return;
				}
			} else {
				return;
			}
			for (int i = 0; i < getCommunicates().size(); i++) {
				if (code != getCommunicates().get(0).what) {
					getCommunicates().remove(0);
				} else {
					i = getCommunicates().size() + 1;
				}
			}
			getCommunicates().remove(0);
			getMessageInterface().removeMessagae();
		} catch (IndexOutOfBoundsException e) {
		} catch (Exception e) {
		}
	}

	@Override
	public void onCharacteristicWrite(BluetoothGattCharacteristic characteristic) {
	}

	@Override
	public void onConnectionStateChange(int newState) {
		if (newState == BluetoothProfile.STATE_DISCONNECTED) {
			clearMessages();
		}
	}

	// /
	/** Clear all messages */
	public void clearMessages() {
		Util.sLog(TAG, "clearMessages:" + getCommunicates().size(), "e", D);
		getCommunicates().clear();
	}

	/**
	 * @return the communicates
	 */
	public List<Message> getCommunicates() {
		return communicates;
	}

	/**
	 * @return the messageInterface
	 */
	public MessageInterface getMessageInterface() {
		return messageInterface;
	}

	/**
	 * @param messageInterface
	 *            the messageInterface to set
	 */
	public void setMessageInterface(MessageInterface messageInterface) {
		this.messageInterface = messageInterface;
	}

	/**
	 * @return the sucessOnce
	 */
	public boolean isSucessOnce() {
		return sucessOnce;
	}

	/**
	 * @param sucessOnce
	 *            the sucessOnce to set
	 */
	public void setSucessOnce(boolean sucessOnce) {
		this.sucessOnce = sucessOnce;
	}

}
