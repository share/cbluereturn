package com.ble;

import com.until.Constants;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * 监听蓝牙开关
 * @author zhangbo
 *
 */
public class BluetoothChangeReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();  
		  
        if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action))  
        {  
           int bluetooth_state = BluetoothAdapter.getDefaultAdapter().getState();
           if(bluetooth_state == BluetoothAdapter.STATE_OFF) {
        	   Log.i("zhangbo", "20140103----蓝牙关闭");
        	   BluetoothLeServer.setNullForBluetoothLeServer();
           }
           else if (bluetooth_state == BluetoothAdapter.STATE_TURNING_ON) {
        	   Log.i("zhangbo", "20140103----蓝牙STATE_TURNING_ON");
           }
           else if (bluetooth_state == BluetoothAdapter.STATE_ON) {
        	   Log.i("zhangbo", "20140103----蓝牙开启");
        	   Intent intentForStateOn = new Intent(Constants.ACTION_GATT_STATE_ON);
        	   context.getApplicationContext().sendBroadcast(intentForStateOn);
           }
        }
	}

}
