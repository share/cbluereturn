package com.ble;

import android.content.Context;

/***
 * Bluetooth operations for paxvape
 * 
 * @author dangsang
 * @version 1.0
 * @since 2014.8.5
 * 
 */
public class BleCdsPax extends BleCds {

	protected BleCdsPax(Context context) {
		super(context);
	}

	/** Singleton */
	public static BleCdsPax instance(Context context) {
		if (instance == null) {
			instance = new BleCdsPax(context);
		}
		return (BleCdsPax) instance;
	}

	/** The device is available for applicaion */
	public boolean isOk(byte[] bytes) {
		if (bytes[1] == 0x02)
			return true;
		return false;
	}

	/** Query the device is available for what kind of application */
	public void queryAppType() {
		byte[] set = { Commands.RAPP_TYPE };
		BluetoothLeServer.getInstance(context).write(set);
	}

	/** Check the device heating state */
	public boolean isHeating(byte[] bytes) {
		if (bytes[1] == 0x01)
			return true;
		return false;
	}

	/** Query heat state */
	public void queryHeatState() {
		byte[] set = { Commands.RHEAL_STATE };
		BluetoothLeServer.getInstance(context).write(set);
	}

	/** Get the time counted of device need to less than 65792 */
	public int getTimeCounted(byte[] bytes) {
		return (bytes[1] & 0xff) * 256 + (0xff & bytes[2]);
	}

	/** Get the current time counted */
	public void queryCountTimeAt() {
		byte[] set = { Commands.RCOUNT_START };
		BluetoothLeServer.getInstance(context).write(set);
	}

	/** Get temperature sent from device */
	public int getTemperature(byte[] bytes) {
		return (bytes[1] & 0xff) * 256 + (0xff & bytes[2]);
	}

	/** Stop heating */
	public void queryStopHeat() {
		// 停止加热请求
		byte[] set = { Commands.RSTOP_HEAL };
		BluetoothLeServer.getInstance(context).write(set);
	}

	/** Set temperature and time to device */
	public void setTemperatureAndTime(int temp, int time) {
		byte[] set = new byte[5];
		byte[] bytes;
		set[0] = Commands.RTEM_SET;
		bytes = toByte(temp, 2);
		set[1] = bytes[0];
		set[2] = bytes[1];
		bytes = toByte(time, 2);
		set[3] = bytes[0];
		set[4] = bytes[1];
		BluetoothLeServer.getInstance(context).write(set);
	}

	/** Check the return code is questing for */
	public boolean isQuested(int recode) {
		switch (recode) {
		case Commands.ATEMP:// auto send by device
			return false;
		default:
			return true;
		}
	}

	/** Check the return code is auto sent from device */
	public boolean isAuto(int recode) {
		switch (recode) {
		case Commands.ATEMP:// auto send by device
		case Commands.TCOUNT_START:
		case BleCds.Commands.TBATTERY:
			return true;
		default:
			return false;
		}
	}

	/** Get the return code of request */
	public int getReturnCode(int request) {
		int ret = -1;
		switch (request) {
		case BleCds.Commands.RTIME:
			ret = BleCds.Commands.TTIME;
			break;
		case Commands.RSTOP_HEAL:
			ret = Commands.TSTOP_HEAL;
			break;
		case BleCds.Commands.RBATTERY:
			ret = BleCds.Commands.TBATTERY;
			break;
		case Commands.RTEM_SET:
			ret = Commands.TTEM_SET;
			break;
		case Commands.RCOUNT_START:
			ret = Commands.TCOUNT_START;
			break;
		case BleCds.Commands.RID:
			ret = BleCds.Commands.TID;
			break;
		case Commands.RHEAL_STATE:// auto send by device
			ret = Commands.THEAL_STATE;
			break;
		case BleCds.Commands.RNAME:
			ret = BleCds.Commands.TNAME;
			break;
		case BleCds.Commands.RCHARGED:
			ret = BleCds.Commands.TCHARGED;
			break;
		case BleCds.Commands.RPASSWORD_SET:
			ret = BleCds.Commands.TPASSWORD_SET;
			break;
		case BleCds.Commands.RPASSWORD_READ:
			ret = BleCds.Commands.TPASSWORD_READ;
			break;
		case Commands.RAPP_TYPE:
			ret = Commands.TAPP_TYPE;
			break;
		default:
			ret = 0x00;
			break;
		}
		return ret;
	}

	public static class Commands {
		// retuest
		public static final int RSTOP_HEAL = 0x32;
		public static final int RTEM_SET = 0x34;
		public static final int RCOUNT_START = 0x36;
		public static final int RHEAL_STATE = 0x38;
		public static final int RAPP_TYPE = 0x42;

		// /return
		public static final int TSTOP_HEAL = 0x82;
		public static final int TTEM_SET = 0x84;
		public static final int TCOUNT_START = 0x86;
		public static final int THEAL_STATE = 0x88;
		public static final int TAPP_TYPE = 0x92;
		// /auto
		public static final int ATEMP = 0x85;

	}

}
