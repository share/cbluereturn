package com.ble;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.content.Context;

import com.bean.DayData;
import com.until.Util;

/**
 * @author dangsang
 * @version 1.0
 * @since 2014.8.1
 *        <p>
 *        This is the static commands between device and ble
 */

public class BleCds {
	protected static BleCds instance;
	Context context;

	protected BleCds(Context context) {
		this.context = context.getApplicationContext();
	}

	/** Singleton */
	public static BleCds instance(Context context) {
		if (instance == null) {
			instance = new BleCds(context);
		}
		return instance;
	}

	/** Query plan data from device */
	public void queryPlan() {
		byte[] getPlanDataBytes = { Commands.RPLAN_READ };
		BluetoothLeServer.getInstance(context).write(getPlanDataBytes);
	}

	/** Unlimit the plan limit */
	public void unlimitPlan() {
		byte[] unlockPlanLimitBytes = { Commands.RUNLIMIT };
		BluetoothLeServer.getInstance(context).write(unlockPlanLimitBytes);
	}

	/** Get charged times to device */
	public int getChargedNum(byte[] bytes) {
		return (bytes[1] & 0xFF) * 256 + (bytes[2] & 0xFF);
	}

	/** Get battery of device */
	public int getBattery(byte[] bytes) {
		return (bytes[1] & 0xFF) * 256 + (bytes[2] & 0xFF);
	}

	/** Get plan value as DayData see {@link DayData} */
	public List<DayData> getDevicePlanData(byte[] bytes) {
		// 获取日期
		int day = (bytes[1] & 0xFF);
		int month = (bytes[2] & 0xFF);
		int year = (bytes[3] & 0xFF);
		String strYear = "20" + year;
		String strMonth = "" + month;
		String strDay = "" + day;
		if (strMonth.length() == 1) {
			strMonth = "0" + strMonth;
		}
		if (strDay.length() == 1) {
			strDay = "0" + strDay;
		}
		String startDate = strYear + strMonth + strDay;
		ArrayList<DayData> planList = new ArrayList<DayData>();
		for (int i = 0; i < 7; i++) {
			if ((bytes[2 * i + 4] & 0xFF) != 255
					|| (bytes[2 * i + 5] & 0xFF) != 255) {
				int planValue = (bytes[2 * i + 4] & 0xFF) * 256
						+ (bytes[2 * i + 5] & 0xFF);
				String dateStr = Util.getDifferDate(startDate, i);
				DayData dayData = new DayData();
				dayData.setDate(Integer.valueOf(dateStr)); // 日期
				dayData.setNum_plan(planValue); // 计划值
				planList.add(dayData);
			}
		}
		return planList;
	}

	/** Set plan data to device see {@link DayData} */
	public void setPlanData(List<DayData> list) {
		if (list.size() != 0 && list.get(0).getNum_plan() != -1) {
			List<DayData> sevenList = new ArrayList<DayData>();
			// 如果有计划值，获取7天计划值
			for (int i = 0; i < list.size(); i++) {
				if (i > 6) {
					break;
				}
				sevenList.add(list.get(i));
			}
			byte[] planValuebytes = new byte[15];
			planValuebytes[0] = Commands.RPASSWORD_SET;
			int num = 1;
			for (int i = 0; i < sevenList.size(); i++) {
				byte[] fomateByte = toByte(sevenList.get(i).getNum_plan(), 2);
				planValuebytes[num] = fomateByte[0];
				num++;
				planValuebytes[num] = fomateByte[1];
				num++;
			}
			for (int i = num; i < 15; i++) {
				planValuebytes[i] = (byte) 0xFF;
			}
			// 将数据传给设备
			BluetoothLeServer.getInstance(context).write(planValuebytes);
		}
	}

	/** Query battery of device */
	public void queryDeviceBattery() {
		byte[] qeuryBatteryBytes = { Commands.RBATTERY };
		BluetoothLeServer.getInstance(context).write(qeuryBatteryBytes);
	}

	/** Query charged times */
	public void queryChargeNum() {
		byte[] qeuryBatteryBytes = { Commands.RCHARGED };
		BluetoothLeServer.getInstance(context).write(qeuryBatteryBytes);
	}

	/** Get current power of device */
	public int getPower(byte[] bytes) {
		return (bytes[1] & 0xFF);
	}

	/** Get current resistance of device */
	public int getResistance(byte[] bytes) {
		return (bytes[2] & 0xFF);
	}

	/** Get current voltage of device */
	public int getVoltage(byte[] bytes) {
		return (bytes[3] & 0xFF);
	}

	/** Set smog quantity */
	public void setDeviceSmogQuantity(int smogQuantityInt) {
		byte[] setsmogQuantityBytes = { Commands.RSMOG, (byte) smogQuantityInt };
		BluetoothLeServer.getInstance(context).write(setsmogQuantityBytes);
	}

	/** Query for device id */
	public void queryId() {
		// 获取设备id
		byte[] qeuryID = { Commands.RID };
		BluetoothLeServer.getInstance(context).write(qeuryID);
	}

	/** Set the password to device */
	public void setDeviceName(String name) {
		byte[] deviceNameBytes = new byte[16];
		deviceNameBytes[0] = Commands.RNAME;
		for (int i = 0; i < 15; i++) {
			if (i < name.length()) {
				deviceNameBytes[i + 1] = (byte) name.charAt(i);
			} else {
				deviceNameBytes[i + 1] = (byte) 0xFF;
			}
		}
		// 将数据传给设备
		BluetoothLeServer.getInstance(context).write(deviceNameBytes);
	}

	/** Set the password to device */
	public void setPassword(String password) {
		byte[] devicePasswordBytes = new byte[7];
		devicePasswordBytes[0] = Commands.RPASSWORD_SET;
		for (int i = 0; i < 6; i++) {
			if (i < password.length()) {
				devicePasswordBytes[i + 1] = (byte) password.charAt(i);
			} else {
				devicePasswordBytes[i + 1] = (byte) 0xFF;
			}
		}
		// 将数据传给设备
		BluetoothLeServer.getInstance(context).write(devicePasswordBytes);
	}

	/** Query for password */
	public void queryPassword() {
		byte[] qeuryPassword = { Commands.RPASSWORD_READ };
		BluetoothLeServer.getInstance(context).write(qeuryPassword);
	}

	/** Get password from byte array */
	public String getPassword(byte[] bytes) {
		String password = "";
		int byteValue = (bytes[1] & 0xFF);
		if (byteValue == 255)
			return password;
		// 解析密码,第2个到第7个byte是密码
		for (int i = 1; i < 7; i++) {
			byteValue = (bytes[i] & 0xFF);
			if (byteValue == 255) {
				break;
			}
			password = password + (char) byteValue;
		}
		return password;
	}

	/** Clear device data */
	public void clearDeviceData() {
		// 清空数据
		byte[] qeuryID = { Commands.RCLEAR };
		BluetoothLeServer.getInstance(context).write(qeuryID);
	}

	/** Get Firmware version */
	public int getFirmwareVersion(byte[] bytes) {
		return (bytes[1] & 0xFF) * 256 * 256 + (bytes[2] & 0xFF) * 256
				+ (bytes[3] & 0xFF);
	}

	/** Get Firmware type */

	public String getFirmwareType(byte[] bytes) {
		// 板子类型的获取
		String deviceType = "";
		if ((bytes[4] & 0xFF) != 0) {
			deviceType = deviceType + (char) (bytes[4] & 0xFF)
					+ (char) (bytes[5] & 0xFF) + (char) (bytes[6] & 0xFF)
					+ (char) (bytes[7] & 0xFF) + (char) (bytes[8] & 0xFF);
		}
		if (!deviceType.isEmpty())
			return deviceType.toUpperCase(new Locale("en"));
		return deviceType;
	}

	/** Set time to device to device firmware version,frimware type */
	public void setDeviceTime() {
		// 当前时间对象
		Calendar thisCalendar = Calendar.getInstance();
		// 当前年
		int thisYear = thisCalendar.get(Calendar.YEAR);
		int year = Integer.valueOf(String.valueOf(thisYear).substring(2, 4));
		// 当前月(获取月份得加1)
		int thisMonth = thisCalendar.get(Calendar.MONTH) + 1;
		// 当前日
		int thisDay = thisCalendar.get(Calendar.DAY_OF_MONTH);
		int hour = thisCalendar.get(Calendar.HOUR_OF_DAY);
		int minute = thisCalendar.get(Calendar.MINUTE);
		int second = thisCalendar.get(Calendar.SECOND);

		byte[] setTimeBytes = { Commands.RTIME, (byte) second, (byte) minute,
				(byte) hour, (byte) thisDay, (byte) thisMonth, (byte) year };
		BluetoothLeServer.getInstance(context).write(setTimeBytes);
	}

	/** COMMANDS AND RETURN CODES */
	public static class Commands {
		// retuest
		public static final int RTIME = 0x31;
		public static final int RSMOG = 0x32;
		public static final int RBATTERY = 0x33;
		public static final int RCLEAR = 0x34;
		public static final int RDATA = 0x35;
		public static final int RPLAN_SET = 0x36;
		public static final int RUNLIMIT = 0x39;
		public static final int RID = 0x37;
		public static final int RPLAN_READ = 0x3a;
		public static final int RNAME = 0x3b;
		public static final int RCHARGED = 0x3c;
		public static final int RTIME_PRESSED = 0x3d;
		public static final int RPASSWORD_SET = 0x3e;
		public static final int RPASSWORD_READ = 0x3f;
		public static final int RVOL = 0x40;
		// /
		public static final int TTIME = 0x81;
		public static final int TSMOG = 0x82;
		public static final int TBATTERY = 0x83;
		public static final int TCLEAR = 0x84;
		public static final int TDATA = 0x85;
		public static final int TPLAN_SET = 0x86;
		public static final int TUNLIMIT = 0x89;
		public static final int TID = 0x87;
		public static final int TPLAN_READ = 0x8a;
		public static final int TNAME = 0x8b;
		public static final int TCHARGED = 0x8c;
		public static final int TTIME_PRESSED = 0x8d;
		public static final int TPASSWORD_SET = 0x8e;
		public static final int TPASSWORD_READ = 0x8f;
		public static final int TVOL = 0x90;
		// /
		public static final int ASMOKE = 0x88;
	}

	/** Check the return code is questing for */
	public boolean isQuested(int recode) {
		switch (recode) {
		case Commands.ASMOKE:// auto send by device
			return false;
		default:
			return true;
		}
	}

	/** Check the return code is auto sent from device */
	public boolean isAuto(int recode) {
		switch (recode) {
		case Commands.ASMOKE:// auto send by device
		case Commands.TBATTERY:
		case Commands.TSMOG:
			return true;
		default:
			return false;
		}
	}

	/** Get the return code of request */
	public int getReturnCode(int request) {
		int ret = -1;
		switch (request) {
		case Commands.RTIME:
			ret = Commands.TTIME;
			break;
		case Commands.RSMOG:
			ret = Commands.TSMOG;
			break;
		case Commands.RBATTERY:
			ret = Commands.TBATTERY;
			break;
		case Commands.RCLEAR:
			ret = Commands.TCLEAR;
			break;
		case Commands.RDATA:
			ret = Commands.TDATA;
			break;
		case Commands.RPLAN_SET:
			ret = Commands.TPLAN_SET;
			break;
		case Commands.RID:
			ret = Commands.TID;
			break;
		// case 0x38://auto send by device
		// ret = 0x88;
		// break;
		// /new for temperature control
		case Commands.RUNLIMIT:
			ret = Commands.TUNLIMIT;
			break;
		case Commands.RPLAN_READ:
			ret = Commands.TPLAN_READ;
			break;
		case Commands.RNAME:
			ret = Commands.TNAME;
			break;
		case Commands.RCHARGED:
			ret = Commands.TCHARGED;
			break;
		case Commands.RTIME_PRESSED:
			ret = Commands.TTIME_PRESSED;
			break;
		case Commands.RPASSWORD_SET:
			ret = Commands.TPASSWORD_SET;
			break;
		case Commands.RPASSWORD_READ:
			ret = Commands.TPASSWORD_READ;
			break;
		case Commands.RVOL:
			ret = Commands.TVOL;
			break;
		}
		return ret;
	}

	/** Get device id as string */
	public String getDeviceIdStr(byte[] bytes) {
		// 计算设备id
		int deviceIdInt = (bytes[1] & 0xFF) * 256 * 256 * 256
				+ (bytes[2] & 0xFF) * 256 * 256 + (bytes[3] & 0xFF) * 256
				+ (bytes[4] & 0xFF);
		String deviceIdStr = "IE";
		for (int i = 0; i < (6 - String.valueOf(deviceIdInt).length()); i++) {
			deviceIdStr += "0";
		}
		deviceIdStr += String.valueOf(deviceIdInt);
		return deviceIdStr;
	}

	/**
	 * Get the byte array of {@code}value restricted to {@code} type
	 * <P>
	 * The high byte in front
	 * 
	 * @param value
	 *            the integer
	 * @param type
	 *            the length of Byte to a number. Integer less than 4 and bigger
	 *            than 0
	 * @return the byte array of integer. Null for illegealArgument.
	 */
	final byte[] toByte(int value, int type) {
		byte[] ab = new byte[type];
		for (int i = 0; i < type; i++) {
			ab[i] = 0;
		}
		for (int j = 0; j < type; j++) {
			// Get the j+1 position byte of value
			ab[j] = toByte(value, j + 1, type);
		}
		return ab;
	}

	/**
	 * Get the byte at {@code}position in total bytes count {@code}type of
	 * integer {@code}value
	 * 
	 * @param value
	 *            the integer
	 * @param position
	 *            the place in total byte count .The value may be 1...type
	 * @param type
	 *            the byte count for {@code}value
	 * @return the byte value
	 */
	final byte toByte(int value, int position, int type) {
		int MAX = 0x000000ff;
		return (byte) ((value >> ((type - position) * Byte.SIZE)) & MAX);
	}

	/**
	 * 距离指定日期相差天数的日期
	 * 
	 * @param maxDate
	 * @param differ
	 * @return
	 */
	@SuppressLint("SimpleDateFormat")
	final String getDifferDate(String maxDate, int differ) {
		String planSettingStartDate = null;
		SimpleDateFormat myFormatter = new SimpleDateFormat("yyyyMMdd");
		try {
			Date date = myFormatter.parse(maxDate);
			long time = date.getTime();
			time += differ * 24 * 60 * 60 * 1000;
			planSettingStartDate = getDateByTime(time);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return planSettingStartDate;
	}

	/**
	 * 根据指定timestamp获取日期
	 * 
	 * @param time
	 * @return
	 */
	final String getDateByTime(long time) {

		// 当前时间对象
		Calendar thisCalendar = Calendar.getInstance();
		thisCalendar.setTimeInMillis(time);
		// 当前年
		int thisYear = thisCalendar.get(Calendar.YEAR);
		// 当前月(获取月份得加1)
		int thisMonth = thisCalendar.get(Calendar.MONTH) + 1;
		// 当前日
		int thisDay = thisCalendar.get(Calendar.DAY_OF_MONTH);

		String strYear = "" + thisYear;
		String strMonth = "" + thisMonth;
		if (strMonth.length() == 1) {
			strMonth = "0" + strMonth;
		}
		String strDay = "" + thisDay;
		if (strDay.length() == 1) {
			strDay = "0" + strDay;
		}

		String dateStr = strYear + strMonth + strDay;

		return dateStr;
	}
}
