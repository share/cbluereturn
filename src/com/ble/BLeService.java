package com.ble;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import android.annotation.TargetApi;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothProfile;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;

import com.ble.BluetoothLeServer.MessageInterface;
import com.until.Constants;
import com.until.PreferenceHelper;
import com.until.StringUtils;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
public class BLeService extends Service implements MessageInterface {
	/*
	 * Copyright (C) 2013 The Android Open Source Project
	 * 
	 * Licensed under the Apache License, Version 2.0 (the "License"); you may
	 * not use this file except in compliance with the License. You may obtain a
	 * copy of the License at
	 * 
	 * http://www.apache.org/licenses/LICENSE-2.0
	 * 
	 * Unless required by applicable law or agreed to in writing, software
	 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
	 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
	 * License for the specific language governing permissions and limitations
	 * under the License.
	 */
	private final static String TAG = "bleServer";
	public static Map<String, String> deviceMap = new HashMap<String, String>();
	public static List<String> deviceList = new ArrayList<String>();

	BluetoothLeServer mBluetoothLeServer;
	private final IBinder mBinder = new LocalBinder();
	private boolean isScaning = false; // 是否正在搜索
	private Handler mHandler;
	private StopLEScanDeviceThread stopLEScanDeviceThread;

	// /
	private TalkInterface talkInterface;
	MessageConsumer messageConsumer;
	Object locker = new Object();
	{// initial
		setTalkInterface(new TalkInterface() {
			@Override
			public void onCharacteristicWrite(
					BluetoothGattCharacteristic characteristic) {
			}

			@Override
			public void onCharacteristicChanged(
					BluetoothGattCharacteristic characteristic) {
			}

			@Override
			public void onConnectionStateChange(int newState) {
			}
		});
	}

	public interface TalkInterface {
		public void onConnectionStateChange(int newState);

		public void onCharacteristicChanged(
				BluetoothGattCharacteristic characteristic);

		public void onCharacteristicWrite(
				BluetoothGattCharacteristic characteristic);
	}

	@Override
	public void onCreate() {
		super.onCreate();

		mHandler = new Handler();
		stopLEScanDeviceThread = new StopLEScanDeviceThread();
		// /
		messageConsumer = new MessageConsumer();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Service#onStartCommand(android.content.Intent, int, int)
	 */
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		mBluetoothLeServer = BluetoothLeServer
				.getInstance(getApplicationContext());
		mBluetoothLeServer.setMessageInterface(this);
		setTalkInterface(mBluetoothLeServer);
		messageConsumer.setCommunicates(mBluetoothLeServer.getCommunicates());
		messageConsumer.setServer(mBluetoothLeServer);
		messageConsumer.start();

		// 搜索设备
		String serviceMark = "";
		try {
			serviceMark = intent.getStringExtra("serviceMark");
		} catch (NullPointerException e) {
		}

		Log.e("bleServer", "开始搜索-----" + serviceMark);
		if ("search_connect".equals(serviceMark)) {
			if (!isScaning) {
				// Stops scanning after a pre-defined scan period.
				mHandler.removeCallbacks(stopLEScanDeviceThread);
				mHandler.postDelayed(stopLEScanDeviceThread, 15000);

				mBluetoothLeServer.stopLEScanDevice(mLeScanMainCallback);
				mBluetoothLeServer.startLEScanDevice(mLeScanMainCallback);
				isScaning = true;
			}
		} else if ("search".equals(serviceMark)) {
			mBluetoothLeServer.disconnect();
			mHandler.removeCallbacks(stopLEScanDeviceThread);
			mHandler.postDelayed(stopLEScanDeviceThread, 10000);

			mBluetoothLeServer.stopLEScanDevice(mLeScanCallback);
			mBluetoothLeServer.startLEScanDevice(mLeScanCallback);
		} else if ("connect".equals(serviceMark)) {
			mHandler.removeCallbacks(stopLEScanDeviceThread);
			mBluetoothLeServer.stopLEScanDevice(mLeScanCallback);
			broadcastUpdate(Constants.ACTION_GATT_STOP_LE_SCAN); // 停止搜索时通知
			final String deviceAddress = intent.getStringExtra("address");
			// 连接该蓝牙设备
			new Thread(new Runnable() {
				@Override
				public void run() {
					// Log.i("bleServer", "开始连接");
					mBluetoothLeServer.connect(deviceAddress, mGattCallback);
				}
			}).start();
		}

		return super.onStartCommand(intent, flags, startId);
	}

	/**
	 * 停止搜索
	 * 
	 * @author bleServer
	 * 
	 */
	private class StopLEScanDeviceThread implements Runnable {

		@Override
		public void run() {
			mBluetoothLeServer.stopLEScanDevice(mLeScanCallback);
			isScaning = false;
			broadcastUpdate(Constants.ACTION_GATT_STOP_LE_SCAN); // 停止搜索时通知
		}

	}

	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		messageConsumer.release();
		mBluetoothLeServer.close();
	}

	@Override
	public boolean onUnbind(Intent intent) {
		// After using a given device, you should make sure that
		// BluetoothGatt.close() is called such that resources are cleaned up
		// properly. In this particular example, close() is invoked when the UI
		// is disconnected from the Service.
		mBluetoothLeServer.close();
		return super.onUnbind(intent);
	}

	public class LocalBinder extends Binder {
		public BLeService getService() {
			return BLeService.this;
		}
	}

	public boolean connect(final String address) {
		return mBluetoothLeServer.connect(address, mGattCallback);
	}

	public BluetoothLeServer getBluetoothLeServer() {
		return mBluetoothLeServer;
	}

	/**
	 * 搜索设备结果回调
	 */
	private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {

		@Override
		public void onLeScan(final BluetoothDevice device, int rssi,
				byte[] scanRecord) {
			Log.i("bleServer",
					device.getName() + "----------------" + device.getAddress());
			String name = "";
			if (null != device.getName()) {
				char c = device.getName().charAt(device.getName().length() - 1);
				if (((int) c & 0xff) == 253) {
					int indexOf = device.getName().indexOf(c + "");
					name = device.getName().substring(0, indexOf);
				} else {
					name = device.getName();
				}
			}
			// Util.show(name.toCharArray(),
			// "ble name" + name.toCharArray().length + "/" + ((char) 253)
			// + name.charAt(name.length() - 1));
			if (StringUtils.isEmpty(name)) {
				return;
			}
			if (!"#".equals(name.substring(0, 1)) && !"JDF".equals(name)) {
				return;
			}

			if (deviceMap.containsKey(device.getAddress())) {
				return;
			}
			if ("JDF".equals(name)) {
				// 如果已经连接过该电子烟，名称做个标注
				if (device.getAddress().equals(
						PreferenceHelper.get(BLeService.this,
								Constants.ESMOKING_MARK_BLUETOOTH_MAC))) {
					deviceMap.put(
							device.getAddress(),
							// device.getName()
							"IEC"
									+ " "
									+ PreferenceHelper.get(BLeService.this,
											Constants.ESMOKING_MARK_USER_ID));
				} else {
					// deviceMap.put(device.getAddress(), device.getName());
					deviceMap.put(device.getAddress(), "IEC");
				}
			} else {
				deviceMap.put(device.getAddress(), name.substring(1));
			}
			deviceList.add(device.getAddress());
			broadcastUpdate(Constants.ACTION_GATT_DEVICE_SEARCHED); // 搜索到设备时通知
		}
	};

	/**
	 * 搜索设备结果回调
	 */
	private BluetoothAdapter.LeScanCallback mLeScanMainCallback = new BluetoothAdapter.LeScanCallback() {

		@Override
		public void onLeScan(final BluetoothDevice device, int rssi,
				byte[] scanRecord) {
			Log.i("bleServer",
					device.getName() + "----------------" + device.getAddress());
			// 判断是否是之前连接的蓝牙设备
			String oldDeviceAddress = PreferenceHelper.get(BLeService.this,
					Constants.ESMOKLING_BLUETOOTH_MAC);
			Log.i("bleServer", "oldDeviceAddress----------------"
					+ oldDeviceAddress);

			if (device.getAddress().equals(oldDeviceAddress)) {
				mBluetoothLeServer.stopLEScanDevice(mLeScanMainCallback);
				mHandler.removeCallbacks(stopLEScanDeviceThread);
				isScaning = false;
				// 连接该蓝牙设备
				new Thread(new Runnable() {
					@Override
					public void run() {
						Log.i("bleServer", "开始连接");
						mBluetoothLeServer.connect(device.getAddress(),
								mGattCallback);
					}
				}).start();
			}
		}
	};

	// Implements callback methods for GATT events that the app cares about. For
	// example, connection change and services discovered.
	/**
	 * 连接状态，消息接受回调
	 */
	private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
		@Override
		public void onConnectionStateChange(BluetoothGatt gatt, int status,
				int newState) {
			if (newState == BluetoothProfile.STATE_CONNECTED) {
				mBluetoothLeServer
						.setConnectionState(BluetoothLeServer.STATE_BLE_CONNECTED);
				broadcastUpdate(Constants.ACTION_GATT_CONNECTED);
				Log.i(TAG, "Connected to GATT server.");
				Log.i(TAG, "Attempting to start service discovery:"
						+ mBluetoothLeServer.getBluetoothGatt()
								.discoverServices());
			} else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
				mBluetoothLeServer
						.setConnectionState(BluetoothLeServer.STATE_BLE_DISCONNECTED);
				Log.i(TAG, "Disconnected from GATT server.");
				broadcastUpdate(Constants.ACTION_GATT_DISCONNECTED);
			}
			getTalkInterface().onConnectionStateChange(newState);
		}

		@Override
		public void onCharacteristicChanged(BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic) {
			getTalkInterface().onCharacteristicChanged(characteristic);
			// Log.i(TAG,
			// "BluetoothGattCallback.onCharacteristicChanged received: "
			// + characteristic.getValue().toString());
			// Util.show(characteristic.getValue(), "changed");
			broadcastUpdate(Constants.ACTION_DATA_AVAILABLE, characteristic);
		}

		@Override
		public void onCharacteristicRead(BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic, int status) {
			// Log.i(TAG, status + "BluetoothGattCallback.onCharacteristicRead"
			// + characteristic.getValue() + characteristic.getUuid());
			// Util.show(characteristic.getValue(), "read");
			if (status == BluetoothGatt.GATT_SUCCESS) {
				broadcastUpdate(Constants.ACTION_DATA_AVAILABLE, characteristic);
			}
		}

		@Override
		public void onCharacteristicWrite(BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic, int status) {
			// Log.i(TAG, status + "BluetoothGattCallback.onCharacteristicWrite"
			// + characteristic.getValue() + characteristic.getUuid());
			// Util.show(characteristic.getValue(), "write");
			if (status == BluetoothGatt.GATT_SUCCESS) {
				getTalkInterface().onCharacteristicWrite(characteristic);
				broadcastUpdate(Constants.ACTION_DATA_AVAILABLE, characteristic);
			}
		}

		@Override
		public void onDescriptorWrite(BluetoothGatt gatt,
				BluetoothGattDescriptor descriptor, int status) {
			// Log.i(TAG, status + "BluetoothGattCallback.onDescriptorWrite"
			// + descriptor.getValue() + descriptor.getUuid());
			if (status == BluetoothGatt.GATT_SUCCESS) {
				mBluetoothLeServer.setSucessOnce(true);
				mBluetoothLeServer.setConnectionState(//
						BluetoothLeServer.STATE_BLE_ENABLE_WRITE);
				broadcastUpdate(Constants.ACTION_GATT_WHITE);
			}
		}

		@Override
		public void onServicesDiscovered(BluetoothGatt gatt, int status) {
			if (status == BluetoothGatt.GATT_SUCCESS) {
				Log.i(TAG,
						"BluetoothGattCallback.onServicesDiscovered received: "
								+ status);
				// Check service and characteristic
				try {
					mBluetoothLeServer
							.setCharacteristicWRITE(mBluetoothLeServer
									.getBluetoothGatt()
									.getService(
											UUID.fromString(Constants.E_SMOKEING_SERVICE))
									.getCharacteristic(
											UUID.fromString(Constants.E_SMOKEING_WRITE)));
					mBluetoothLeServer
							.setCharacteristicREAD(mBluetoothLeServer
									.getBluetoothGatt()
									.getService(
											UUID.fromString(Constants.E_SMOKEING_SERVICE))
									.getCharacteristic(
											UUID.fromString(Constants.E_SMOKEING_READ)));
					mBluetoothLeServer.setCharacteristicNotification(
							mBluetoothLeServer.getCharacteristicREAD(), true,
							Constants.CLIENT_CHARACTERISTIC_CONFIG);
				} catch (NullPointerException e) {
					e.printStackTrace();
				}
				broadcastUpdate(Constants.ACTION_GATT_SERVICES_DISCOVERED);
			} else {
				Log.i(TAG, "onServicesDiscovered received: " + status);
			}
		}

	};

	/**
	 * 发送广播
	 * 
	 * @param action
	 */
	private void broadcastUpdate(String action) {
		Intent intent = new Intent(action);
		sendBroadcast(intent);
	}

	/**
	 * 发送广播
	 * 
	 * @param action
	 * @param characteristic
	 */
	private void broadcastUpdate(String action,
			BluetoothGattCharacteristic characteristic) {
		Intent intent = new Intent(action);

		// This is special handling for the Heart Rate Measurement profile. Data
		// parsing is carried out as per profile specifications:
		// http://developer.bluetooth.org/gatt/characteristics/Pages/CharacteristicViewer.aspx?u=org.bluetooth.characteristic.heart_rate_measurement.xml
		if (Constants.E_SMOKEING_READ.equals(characteristic.getUuid())) {
			intent.putExtra(Constants.EXTRA_DATA, characteristic.getValue());
		} else {
			intent.putExtra(Constants.EXTRA_DATA, characteristic.getValue());
		}
		sendBroadcast(intent);
	}

	/**
	 * @return the talkInterface
	 */
	public TalkInterface getTalkInterface() {
		return talkInterface;
	}

	/**
	 * @param talkInterface
	 *            the talkInterface to set
	 */
	public void setTalkInterface(TalkInterface talkInterface) {
		this.talkInterface = talkInterface;
	}

	// //need time out callback to remove message????
	class MessageConsumer extends Thread {
		BluetoothLeServer mServer;
		List<Message> communicates;
		private boolean isRunning;
		private boolean isWaiting;
		private boolean isWrited;
		Message message;

		/**
		 * @return the communicates
		 */
		public List<Message> getCommunicates() {
			return communicates;
		}

		/**
		 * @param communicates
		 *            the communicates to set
		 */
		public void setCommunicates(List<Message> communicates) {
			this.communicates = communicates;
		}

		@Override
		public synchronized void start() {
			if (!isRunning()) {
				setRunning(true);
				super.start();
			}
		}

		public void release() {
			setRunning(false);
			if (isWaiting() || isWrited()) {
				setWaiting(false);
				setWrited(false);
				synchronized (locker) {
					locker.notifyAll();
				}
			}
		}

		@Override
		public void run() {
			setName("messaging");
			while (isRunning()) {
				try {
					if (communicates.size() < 1
							|| mBluetoothLeServer.getConnectionState() != BluetoothLeServer.STATE_BLE_ENABLE_WRITE) {
						setWaiting(true);
						setWrited(false);
						synchronized (locker) {
							locker.wait();
						}
					}
					if (isRunning()) {
						message = communicates.get(0);
						mServer.write(message);
						setWrited(true);
						setWaiting(false);
						synchronized (locker) {
							locker.wait();
						}
					}
				} catch (InterruptedException e) {
					setWaiting(false);
					setWaiting(false);
					message = null;
				} catch (Exception e) {
				}
			}
			setRunning(false);
		}

		/**
		 * @param mServer
		 *            the mServer to set
		 */
		public void setServer(BluetoothLeServer mServer) {
			this.mServer = mServer;
		}

		/**
		 * @return the isRunning
		 */
		public boolean isRunning() {
			return isRunning;
		}

		/**
		 * @param isRunning
		 *            the isRunning to set
		 */
		public void setRunning(boolean isRunning) {
			this.isRunning = isRunning;
		}

		/**
		 * @return the isWaiting
		 */
		public boolean isWaiting() {
			return isWaiting;
		}

		/**
		 * @param isWaiting
		 *            the isWaiting to set
		 */
		public void setWaiting(boolean isWaiting) {
			this.isWaiting = isWaiting;
		}

		/**
		 * @return the isWrited
		 */
		public boolean isWrited() {
			return isWrited;
		}

		/**
		 * @param isWrited
		 *            the isWrited to set
		 */
		public void setWrited(boolean isWrited) {
			this.isWrited = isWrited;
		}

	}

	// //
	@Override
	public void addMessagae() {// stop waiting
		if (messageConsumer.isRunning() && messageConsumer.isWaiting()) {
			synchronized (locker) {
				// messageConsumer.notify();
				locker.notify();
			}
		}
	}

	@Override
	public void removeMessagae() {
		if (messageConsumer.getCommunicates().size() < 1) {
			synchronized (locker) {
				locker.notify();
			}
		} else if (messageConsumer.isRunning() && messageConsumer.isWrited()) {
			synchronized (locker) {
				locker.notify();
			}
		}
	}
}
