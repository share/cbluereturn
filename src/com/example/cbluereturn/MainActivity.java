package com.example.cbluereturn;

import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.bean.Watcher;
import com.ble.BLeService;
import com.ble.BluetoothLeServer;
import com.until.Constants;
import com.until.PreferenceHelper;
import com.until.Util;

public class MainActivity extends ActionBarActivity {
	/** Debugging */
	static final String tag = "main.";
	static final boolean D = true;
	Context context;
	// query_head
	// query_content
	// query_input
	// query
	// device_name
	// services
	// history
	// services_container
	// history_container
	TextView query_head;
	TextView query_content;
	EditText query_input;
	View query;
	TextView device_name;
	View services;
	View history;
	View services_container;
	View history_container;
	// /
	ExpandableListView serviceExList;

	/**
	 * 点击链接按钮后，更新蓝牙状态用handler
	 */
	@SuppressLint("HandlerLeak")
	private Handler updateConnectStatusHanlder = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			// 蓝牙状态判断
			if (BluetoothLeServer.STATE_BLE_DISCONNECTED == BluetoothLeServer
					.getInstance(context.getApplicationContext())
					.getConnectionState()) {
				// 如果未连接
				device_name.setTag("disconnect");
				device_name.setTextColor(context.getResources().getColor(
						R.color.gray3));
			} else if (BluetoothLeServer.STATE_BLE_CONNECTING == BluetoothLeServer
					.getInstance(context.getApplicationContext())
					.getConnectionState()
					|| BluetoothLeServer.STATE_BLE_CONNECTED == BluetoothLeServer
							.getInstance(context.getApplicationContext())
							.getConnectionState()) {
				// 连接中（此时显示未连接）
				device_name.setTag("disconnect");
			} else if (BluetoothLeServer.STATE_BLE_ENABLE_WRITE == BluetoothLeServer
					.getInstance(context.getApplicationContext())
					.getConnectionState()) {
				// 已连接
				device_name.setTag("connected");
				// 显示设备id
				device_name.setTextColor(context.getResources().getColor(
						R.color.white));
				device_name.setText(PreferenceHelper.get(context,
						Constants.ESMOKING_USER_NAME));
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		context = this;
		findViews();
		setListeners();
		initData();
	}

	void findViews() {
		query_head = (TextView) findViewById(R.id.query_head);
		query_content = (TextView) findViewById(R.id.query_content);
		query_input = (EditText) findViewById(R.id.query_input);
		query = findViewById(R.id.query);
		device_name = (TextView) findViewById(R.id.device_name);
		services = findViewById(R.id.services);
		history = findViewById(R.id.history);
		services_container = findViewById(R.id.services_container);
		history_container = findViewById(R.id.history_container);
		// /
		serviceExList = (ExpandableListView) findViewById(R.id.serviceExList);
	}

	void setListeners() {
		query.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				String[] strings = query_input.getText().toString().split(" ");
				if(strings.length<1)return;
				byte[] data = new byte[strings.length];
				for (int i = 0; i < strings.length; i++) {// to bytes
					data[i] = (byte) (Integer.valueOf(strings[i]) & 0xff);
				}
				BluetoothLeServer.getInstance(context).write(data);
			}
		});
		services.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				services_container.setVisibility(View.VISIBLE);
				history_container.setVisibility(View.GONE);

			}
		});
		history.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				services_container.setVisibility(View.GONE);
				history_container.setVisibility(View.VISIBLE);
			}
		});
		device_name.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {// Connect if disconnected
				if (BluetoothLeServer.getInstance(context).getConnectionState()//
				== BluetoothLeServer.STATE_BLE_DISCONNECTED) {
					Intent intent = new Intent(context, BLeService.class);
					intent.putExtra("serviceMark", "search_connect");
					startService(intent);

					// 开启计时器，如果5秒内没有连接设备就表示未连接
					Timer timer = new Timer();
					timer.schedule(new TimerTask() {
						public void run() {
							// 通知hanlder处理
							Message msg = updateConnectStatusHanlder
									.obtainMessage();
							updateConnectStatusHanlder.sendMessage(msg);
						}
					}, 15000);
				}
			}
		});

		// this.serviceExList.setAdapter(adapter);

		query_input.addTextChangedListener(new Watcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				String[] strings = query_input.getText().toString().split(" ");
				if (strings.length < 1)
					return;
				if (strings.length > 0) {
					query_head.setText(//
							"0x" + Util.getHexFromIntText(strings[0]));
				}
				if (strings.length > 1) {
					StringBuilder builder = new StringBuilder();
					for (int i = 1; i < strings.length; i++) {
						builder.append(Util.getHexFromIntText(strings[i]));
						if (i < strings.length - 1)
							builder.append("|");
					}
					query_content.setText(builder.toString());
				}
			}
		});
	}

	void initData() {

	}

}
