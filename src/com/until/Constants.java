package com.until;

import java.util.HashMap;

public class Constants {
	// /
	public static final String KEY_TEMPERATURE_NOW = "time now";
	public static final String KEY_TIME_SETED = "time seted";
	public static final String KEY_TEMPERATURE_SETED = "temperature seted";
	public static final String UPDATE_MARK = "13";// update mark num

	public static String ESMOKING_USER_ID = "userid"; // 用户id
	public static String ESMOKING_USER_NAME = "username"; // 设备名称
	public static String ESMOKLING_BLUETOOTH_MAC = "bluetoothMac"; // mac地址
	public static String ESMOKING_MARK_USER_ID = "markUserid"; // 标记设备名称
	public static String ESMOKING_MARK_BLUETOOTH_MAC = "markBluetoothMac"; // 标记mac地址
	// public static String ESMOKING_REMEMBER_PASSWORD = "rememberPassword"; //
	// 记住密码标记 0：不记住；1：记住
	public static String ESMOKING_USER_PASSWORD = "password"; // 用户密码
	public static String ESMOKING_USER_ACCESS = "userAccess"; // 接口验证值
	public static String ESMOKING_TODAY = "today"; // 今天的日期
	public static String ESMOKING_BATTERY_CAPACITY = "batteryCapacity"; // 电池容量

	public static String ESMOKING_IS_FIRST_LOGIN = "isFirstLogin"; // 是否是第一次使用（int）
	public static String ESMOKING_GUIDE_TO_ACTIVITY = "guideToActivity"; // 跳转到画面的标记
	public static String ESMOKING_FIRMWARE_VERSION = "firmwareVersion"; // 电子烟固件版本（int）
	public static String ESMOKING_DEVICE_TYPE = "deviceType"; // 电子烟型号
	public static String ESMOKING_DEVICE_BATTERY_CAPACITY = "deviceBatteryCapacity"; // 电子烟电池容量的计算电压
	public static String ESMOKING_DEVICE_CHARGE_NUM = "deviceChargeNum"; // 电子烟充电次数
	public static String ESMOKING_DEVICE_SET_TIME_NUM = "deviceSetTimeNum"; // 设置时间次数
	public static String ESMOKING_UPLOAD_MARK = "userInfoUploadMark"; // 用户信息上传标记
																		// 0：已上传，非零13位：未上传

	/** 接口地址 */
	public static String ESMOKING_SERVER_ADDRESS = "http://211.149.184.161:8080"; // 服务器地址
	public static String ESMOKING_URL_LOGIN = //
	ESMOKING_SERVER_ADDRESS + "/JK/login"; // 登录验证地址
	public static String ESMOKING_URL_NO_AUTH_LOGIN = //
	ESMOKING_SERVER_ADDRESS + "/JK/noAuthLogin"; // 无验证登录地址
	public static String ESMOKING_URL_UPDATE_PASSWORD = //
	ESMOKING_SERVER_ADDRESS + "/JK/updateNewPassword"; // 修改密码
	public static String ESMOKING_URL_GET_DATA = //
	ESMOKING_SERVER_ADDRESS + "/JK/getTemperatureDataByTime"; // 获取指定日期的数据
	public static String ESMOKING_URL_UPDATE_DATA = //
	ESMOKING_SERVER_ADDRESS + "/JK/updateTemperatureData"; // 上传设备数据
	public static String ESMOKING_URL_GET_ANDROID_VERSION_INFO = //
	ESMOKING_SERVER_ADDRESS + "/JK/getAndroidVersionInfo"; // 获取版本信息

	public static final int ESMOKING_SERVER_REQUEST_ERROR = 101; // 服务器返回异常时，线程返回值

	public static String ACTION_GATT_STOP_LE_SCAN = "com.example.bluetooth.le.STOP_LE_SCAN"; // 通知停止搜索
	public static String ACTION_GATT_DEVICE_SEARCHED = "com.example.bluetooth.le.DEVIDE_SEARCHED";
	public static String ACTION_GATT_CONNECTED = "com.example.bluetooth.le.ACTION_GATT_CONNECTED";
	public static String ACTION_GATT_DISCONNECTED = "com.example.bluetooth.le.ACTION_GATT_DISCONNECTED";
	public static String ACTION_GATT_SERVICES_DISCOVERED = "com.example.bluetooth.le.ACTION_GATT_SERVICES_DISCOVERED";
	public static String ACTION_GATT_WHITE = "com.example.bluetooth.le.ACTION_GATT_WHITE"; // 通知可写
	public static String ACTION_DATA_AVAILABLE = "com.example.bluetooth.le.ACTION_DATA_AVAILABLE";
	public static String ACTION_DATA_TEMPERATURE = "com.example.bluetooth.le.ACTION_DATA_TEMPERATURE";
	public static String EXTRA_DATA = "com.example.bluetooth.le.EXTRA_DATA";
	public static String ACTION_GATT_STATE_ON = "com.example.bluetooth.STATE_ON"; // 蓝牙打开的通知
	public static String ACTION_LOGIN_NO_AUTH = "com.example.broadcast.action.noAuthLogin"; // 无验证登录的通知
	public static String ACTION_FINISH_ACTIVITY = "com.example.broadcast.action.finishActivity"; // 关闭activity的通知

	private static HashMap<String, String> attributes = new HashMap<String, String>();
	public static String E_SMOKEING_READ = "00005303-0000-0041-4c50-574953450000";// 8*4*4
	public static String E_SMOKEING_WRITE = "00005302-0000-0041-4c50-574953450000";// 8*4*4
	public static String E_SMOKEING_SERVICE = "00005301-0000-0041-4c50-574953450000";
	public static String CLIENT_CHARACTERISTIC_CONFIG = "00002902-0000-1000-8000-00805f9b34fb";

	static {
		// Sample Services.
		attributes.put("00005301-0000-0041-4c50-574953450000",
				"Electric Smoking Service");
		attributes.put("00001800-0000-1000-8000-00805f9b34fb",
				"Device Information Service");
		// Sample Characteristics.
		attributes.put(E_SMOKEING_READ, "Electric Smoking Read");
		attributes.put(E_SMOKEING_WRITE, "Electric Smoking Write");
		attributes.put("00002a00-0000-1000-8000-00805f9b34fb",
				"Device Name String");
		attributes.put("00002a29-0000-1000-8000-00805f9b34fb",
				"Manufacturer Name String");
	}

	public static String lookup(String uuid, String defaultName) {
		String name = attributes.get(uuid);
		return name == null ? defaultName : name;
	}
}
