package com.until;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Matrix;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

/**
 * Utility class for general
 * 
 * @author duanfs
 * 
 */
public class Util {
	/**
	 * Debugging
	 */
	private static final String TAG = "Util";

	/**
	 * Show the context of matrix.
	 * 
	 * @param matrix
	 */
	public static void show(Matrix matrix) {
		float[] values = new float[9];
		matrix.getValues(values);
		String str = "";
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				str += String.format("%8.2f", values[i * 3 + j]);
			}
			Log.d(TAG + i, str);
			str = "";
		}
	}

	/**
	 * Show the content of a char array.
	 * 
	 * @param chars
	 *            the char array.
	 */
	public static void show(char[] chars, String info) {
		for (int i = 0; i < chars.length; i++) {
			Log.i(info + "[" + i + "]",
					chars[i] + String.format("=%2d", (0xff & (byte) chars[i])));
		}
		// Log.d(TAG, str);
	}

	/**
	 * Show each element of array
	 * 
	 * @param bytes
	 *            the array to show
	 */
	public static void show(byte[] bytes) {
		if (null == bytes)
			return;
		int l = 0;
		for (int i = 0; i < bytes.length; i++) {
			l = (bytes[i] & 0xFF);
			Log.i(TAG + "[" + i + "]",
					"0x"
							+ String.format("%2s", Integer.toHexString(l)
									.toUpperCase(new Locale("en"))) + "= " + l);
		}
	}

	public static void show(int[] array) {
		if (null == array)
			return;
		int l = 0;
		for (int i = 0; i < array.length; i++) {
			l = array[i];
			Log.i(TAG + "[" + i + "]",
					"0x"
							+ String.format("%2s", Integer.toHexString(l)
									.toUpperCase(new Locale("en"))) + "= " + l);
		}
	}

	/**
	 * Show each element of array
	 * 
	 * @param bytes
	 *            the array to show
	 */
	public static void show(byte[] bytes, String info) {
		if (null == bytes) {
			return;
		}
		int l = 0;
		for (int i = 0; i < bytes.length; i++) {
			l = (bytes[i] & 0xFF);
			// Log.i(TAG + "[" + i + "]" + info,
			// "0x"+ String.format(
			// "%2s=%3d[%2c]",
			// Integer.toHexString(l).toUpperCase(new Locale("en")),
			// l,
			// l));
			//
			Log.i(info + "[" + i + "]",
					"十进制："
							+ l
							+ "------十六进制："
							+ "0x"
							+ String.format("%2s", Integer.toHexString(l)
									.toUpperCase(new Locale("en"))));

		}
	}

	/**
	 * Show each element of array start at offset to offset+count.
	 * 
	 * @param bytes
	 *            the byte array
	 * @param offset
	 *            the start position to show
	 * @param count
	 *            the count to show
	 */
	public static void show(byte[] bytes, int offset, int count, String info) {
		if (null == bytes) {
			return;
		}
		if (offset + count > bytes.length) {
			count = bytes.length - offset;
		}
		int l = 0;
		for (int i = offset; i < offset + count; i++) {
			// Log.i(TAG + "[" + i + "]" + info, (bytes[i] & 0xFF) + "");
			l = (bytes[i] & 0xFF);
			Log.i(info + "[" + i + "]",
					"0x"
							+ String.format("%2s", Integer.toHexString(l)
									.toUpperCase(new Locale("en"))) + "= " + l);
		}
	}

	/**
	 * Check the bounds of array
	 * 
	 * @param arrayLength
	 *            the length of array.
	 * @param offset
	 *            the start position in {@code buffer} from where to get bytes.
	 * @param count
	 *            the number of bytes from {@code buffer} to write to this data.
	 * @throws IndexOutOfBoundsException
	 *             if {@code offset < 0} or {@code count < 0}, or if
	 *             {@code offset + count} is bigger than the length of
	 *             {@code buffer}.
	 */
	public static final void checkBoundsofArray(int arrayLength, int offset,
			int count) {
		if ((offset | count) < 0 || offset > arrayLength
				|| arrayLength - offset < count) {
			throw new IndexOutOfBoundsException("length=" + arrayLength
					+ "; regionStart=" + offset + "; regionLength=" + count);
		}
	}

	/**
	 * 设备id名称的计算
	 * 
	 * @param deviceIdInt
	 * @return
	 */
	public static String getDeviceIdStr(int deviceIdInt) {

		String deviceIdStr = "IE";

		for (int i = 0; i < (6 - String.valueOf(deviceIdInt).length()); i++) {
			deviceIdStr += "0";
		}

		deviceIdStr += String.valueOf(deviceIdInt);

		return deviceIdStr;
	}

	/**
	 * 获取今天的日期
	 * 
	 * @return
	 */
	public static String getToday() {

		long systemTime = System.currentTimeMillis();
		String dateStr = getDateByTime(systemTime);

		return dateStr;
	}

	/**
	 * 获取距离今天相差指定天数的日期
	 * 
	 * @param differ
	 *            相差的天数
	 * @return
	 */
	public static String getSpecifyDay(int differ) {

		long systemTime = System.currentTimeMillis();
		long specifyTime = systemTime + differ * 24 * 3600 * 1000;

		String dateStr = getDateByTime(specifyTime);

		return dateStr;
	}

	/**
	 * 根据指定日期获取到今天的所有日期
	 * 
	 * @param maxDate
	 *            例："20131218"
	 * @return
	 */
	@SuppressLint("SimpleDateFormat")
	public static List<String> getMaxDateToToday(String maxDate) {
		List<String> list = new ArrayList<String>();

		long systemTime = System.currentTimeMillis();
		SimpleDateFormat myFormatter = new SimpleDateFormat("yyyyMMdd");
		try {
			Date date = myFormatter.parse(maxDate);
			long time = date.getTime();
			while (time < systemTime) {
				String oneDate = getDateByTime(time);
				list.add(oneDate);

				time += 24 * 60 * 60 * 1000;
			}

		} catch (ParseException e) {
			e.printStackTrace();
		}

		return list;
	}

	/**
	 * 距离指定日期相差天数的日期
	 * 
	 * @param maxDate
	 * @param differ
	 * @return
	 */
	@SuppressLint("SimpleDateFormat")
	public static String getDifferDate(String maxDate, int differ) {
		String planSettingStartDate = null;
		SimpleDateFormat myFormatter = new SimpleDateFormat("yyyyMMdd");
		try {
			Date date = myFormatter.parse(maxDate);
			long time = date.getTime();
			time += differ * 24 * 60 * 60 * 1000;
			planSettingStartDate = getDateByTime(time);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return planSettingStartDate;
	}

	/**
	 * 根据指定timestamp获取日期
	 * 
	 * @param time
	 * @return
	 */
	public static String getDateByTime(long time) {

		// 当前时间对象
		Calendar thisCalendar = Calendar.getInstance();
		thisCalendar.setTimeInMillis(time);
		// 当前年
		int thisYear = thisCalendar.get(Calendar.YEAR);
		// 当前月(获取月份得加1)
		int thisMonth = thisCalendar.get(Calendar.MONTH) + 1;
		// 当前日
		int thisDay = thisCalendar.get(Calendar.DAY_OF_MONTH);

		String strYear = "" + thisYear;
		String strMonth = "" + thisMonth;
		if (strMonth.length() == 1) {
			strMonth = "0" + strMonth;
		}
		String strDay = "" + thisDay;
		if (strDay.length() == 1) {
			strDay = "0" + strDay;
		}

		String dateStr = strYear + strMonth + strDay;

		return dateStr;
	}

	/**
	 * 转换时间格式
	 * 
	 * @param dateStr
	 * @return
	 */
	@SuppressLint("SimpleDateFormat")
	public static String formatDate(String dateStr) {
		String dateResultString = "";
		SimpleDateFormat myFormatter = new SimpleDateFormat("yyyyMMdd");
		try {
			Date date = myFormatter.parse(dateStr);

			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			dateResultString = formatter.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return dateResultString;
	}

	/**
	 * 转换时间格式
	 * 
	 * @param dateStr
	 * @return
	 */
	@SuppressLint("SimpleDateFormat")
	public static String formatDateWest(String dateStr) {
		String dateResultString = "";
		SimpleDateFormat myFormatter = new SimpleDateFormat("yyyyMMdd");
		try {
			Date date = myFormatter.parse(dateStr);

			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			dateResultString = formatter.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return dateResultString;
	}

	/**
	 * 转换时间格式
	 * 
	 * @param dateStr
	 * @return
	 */
	@SuppressLint("SimpleDateFormat")
	public static String formatDateTwo(String dateStr) {
		String dateResultString = "";
		SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Date date = myFormatter.parse(dateStr);

			SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
			dateResultString = formatter.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return dateResultString;
	}

	/**
	 * 是否有可用网络
	 * 
	 * @param context
	 * @return
	 */
	public static boolean isAvailableNetwork(Context context) {
		boolean available = false;

		ConnectivityManager connectivitymanager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkinfo = connectivitymanager
				.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);// 手机数据连接
		NetworkInfo wifinetinfo = connectivitymanager
				.getNetworkInfo(ConnectivityManager.TYPE_WIFI);// WIFI

		if ((networkinfo != null && networkinfo.isConnected())
				|| (wifinetinfo != null && wifinetinfo.isConnected())) {
			available = true;
		}

		return available;
	}

	/** log out */
	public static void sLog(String tag, String str, String pattern, boolean D) {
		if (pattern.equals("d"))
			if (D)
				Log.d(tag, str);
		if (pattern.equals("e"))
			if (D)
				Log.e(tag, str);
		if (pattern.equals("w"))
			if (D)
				Log.w(tag, str);
		if (pattern.equals("i"))
			if (D)
				Log.i(tag, str);
	}

	/** Toast string */
	public static void toast(Context cont, int stringId, int time) {
		Toast.makeText(cont, stringId, time).show();
	}

	public static void toast(Context context, String string, int lengthShort) {
		Toast.makeText(context, string, lengthShort).show();
	}

	/** Check if string is null and whitespace */
	public static boolean isNullOrWhitespace(String str) {
		if (null != str && str.trim().length() > 0)
			return false;
		return true;
	}

	/** Hide input soft keyboard */
	public static final void hideKeyboard(Context context) {
		// Set keyboard gone
		InputMethodManager imm = (InputMethodManager) context
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
	}

	// /
	/** Get day long time */
	public static long getTrimDayLong(long time) {
		// 当前时间对象
		Calendar calendar = new GregorianCalendar();
		calendar.setTimeInMillis(time);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTimeInMillis();
	}

	/** Get day long time from 20140811 like */
	public static long getDayLongFromDate(String sDate) {
		// 当前时间对象
		Calendar calendar = new GregorianCalendar();
		calendar.set(Calendar.YEAR, Integer.valueOf(sDate.substring(0, 4)));
		calendar.set(Calendar.MONTH, Integer.valueOf(sDate.substring(4, 6)) - 1);
		calendar.set(Calendar.DAY_OF_MONTH,
				Integer.valueOf(sDate.substring(6, 8)));
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTimeInMillis();
	}

	/** Get day long time from 20140811 like */
	public static long getDayLongFromIntDate(int date) {
		String sDate = String.valueOf(date);
		// 当前时间对象
		Calendar calendar = new GregorianCalendar();
		calendar.set(Calendar.YEAR, Integer.valueOf(sDate.substring(0, 4)));
		calendar.set(Calendar.MONTH, Integer.valueOf(sDate.substring(4, 6)) - 1);
		calendar.set(Calendar.DAY_OF_MONTH,
				Integer.valueOf(sDate.substring(6, 8)));
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTimeInMillis();
	}

	/** To hex string from int string */
	public static final String getHexFromIntText(String value) {
		return String.format("%2s", Integer.toHexString(Integer.valueOf(value))
				.toUpperCase(new Locale("en")));
	}
}
