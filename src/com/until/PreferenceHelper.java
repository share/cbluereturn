package com.until;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * SharePreference的帮助类
 * 
 * @author zhangbo
 * 
 */
public class PreferenceHelper {

	public static final String PREFS_NAME = "esomking";

	/**
	 * 
	 * 获取配置文件
	 * 
	 * @param ctx
	 * @param param
	 * @return
	 */
	public static String get(Context ctx, String param) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFS_NAME, 0);
		String result = settings.getString(param, "");
		return result;
	}

	/**
	 * 删除配置文件
	 * 
	 * @param ctx
	 * @param param
	 */
	public static void removePreference(Context ctx, String param) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFS_NAME,
				Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		editor.remove(param);
		editor.commit();
	}

	/**
	 * 添加配置文件
	 * 
	 * @param ctx
	 * @param param
	 * @param value
	 */
	public static void setParam(Context ctx, String param, String value) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFS_NAME,
				Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(param, value);
		editor.commit();
	}

	/**
	 * 添加配置文件(boolean)
	 * 
	 * @param ctx
	 * @param param
	 * @param value
	 */
	public static void setBooleanParam(Context ctx, String param, boolean value) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFS_NAME,
				Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean(param, value);
		editor.commit();
	}

	/**
	 * 
	 * 获取配置文件(boolean)
	 * 
	 * @param ctx
	 * @param param
	 * @return
	 */
	public static boolean getBoolean(Context ctx, String param) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFS_NAME, 0);
		boolean result = settings.getBoolean(param, false);
		return result;
	}

	/**
	 * 添加配置文件(int)
	 * 
	 * @param ctx
	 * @param param
	 * @param value
	 */
	public static void setIntParam(Context ctx, String param, int value) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFS_NAME,
				Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt(param, value);
		editor.commit();
	}

	/**
	 * 
	 * 获取配置文件(int)
	 * 
	 * @param ctx
	 * @param param
	 * @return
	 */
	public static int getInt(Context ctx, String param) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFS_NAME, 0);
		int result = settings.getInt(param, 0);
		return result;
	}

	/**
	 * 添加配置文件(float)
	 * 
	 * @param ctx
	 * @param param
	 * @param value
	 */
	public static void setFloatParam(Context ctx, String param, float value) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFS_NAME,
				Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		editor.putFloat(param, value);
		editor.commit();
	}

	/**
	 * 
	 * 获取配置文件(float)
	 * 
	 * @param ctx
	 * @param param
	 * @return
	 */
	public static float getFloat(Context ctx, String param) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFS_NAME, 0);
		float result = settings.getFloat(param, 0);
		return result;
	}

	/**
	 * 添加配置文件(long)
	 * 
	 * @param ctx
	 * @param param
	 * @param value
	 */
	public static void setLongParam(Context ctx, String param, long value) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFS_NAME,
				Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		editor.putLong(param, value);
		editor.commit();
	}

	/**
	 * 
	 * 获取配置文件(long)
	 * 
	 * @param ctx
	 * @param param
	 * @return
	 */
	public static long getLong(Context ctx, String param) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFS_NAME, 0);
		return settings.getLong(param, 0l);
	}
}
