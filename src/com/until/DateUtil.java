package com.until;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import android.annotation.SuppressLint;

import com.example.cbluereturn.R;

public class DateUtil {
	/**
	 * 获取今天的日期
	 * 
	 * @return
	 */
	public static String getToday() {

		long systemTime = System.currentTimeMillis();
		String dateStr = getDateByTime(systemTime);

		return dateStr;
	}

	/**
	 * 获取当前时间
	 * @return
	 */
	@SuppressLint("SimpleDateFormat")
	public static String getNowTime() {
		String strReturn = "";
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			strReturn = sdf.format(new Date());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return strReturn;
	}
	
	/**
	 * 获取当前时间
	 * @return
	 */
	@SuppressLint("SimpleDateFormat")
	public static String getNowTimeWest() {
		String strReturn = "";
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			strReturn = sdf.format(new Date());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return strReturn;
	}
	
	/**
	 * 获取距离今天相差指定天数的日期
	 * 
	 * @param differ
	 *            相差的天数
	 * @return
	 */
	public static String getSpecifyDay(int differ) {

		long systemTime = System.currentTimeMillis();
		long specifyTime = systemTime + differ * 24 * 3600 * 1000;

		String dateStr = getDateByTime(specifyTime);

		return dateStr;
	}

	/**
	 * 根据指定日期获取到今天的所有日期
	 * 
	 * @param maxDate
	 *            例："20131218"
	 * @return
	 */
	@SuppressLint("SimpleDateFormat")
	public static List<String> getMaxDateToToday(String maxDate) {
		List<String> list = new ArrayList<String>();

		long systemTime = System.currentTimeMillis();
		SimpleDateFormat myFormatter = new SimpleDateFormat("yyyyMMdd");
		try {
			Date date = myFormatter.parse(maxDate);
			long time = date.getTime();
			while (time < systemTime) {
				String oneDate = getDateByTime(time);
				list.add(oneDate);

				time += 24 * 60 * 60 * 1000;
			}

		} catch (ParseException e) {
			e.printStackTrace();
		}

		return list;
	}

	/**
	 * 距离指定日期相差天数的日期
	 * 
	 * @param maxDate
	 * @param differ
	 * @return
	 */
	@SuppressLint("SimpleDateFormat")
	public static String getDifferDate(String maxDate, int differ) {
		String planSettingStartDate = null;
		SimpleDateFormat myFormatter = new SimpleDateFormat("yyyyMMdd");
		try {
			Date date = myFormatter.parse(maxDate);
			long time = date.getTime();
			time += differ * 24 * 60 * 60 * 1000;
			planSettingStartDate = getDateByTime(time);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return planSettingStartDate;
	}

	/**
	 * 根据指定timestamp获取日期
	 * 
	 * @param time
	 * @return
	 */
	public static String getDateByTime(long time) {

		// 当前时间对象
		Calendar thisCalendar = Calendar.getInstance();
		thisCalendar.setTimeInMillis(time);
		// 当前年
		int thisYear = thisCalendar.get(Calendar.YEAR);
		// 当前月(获取月份得加1)
		int thisMonth = thisCalendar.get(Calendar.MONTH) + 1;
		// 当前日
		int thisDay = thisCalendar.get(Calendar.DAY_OF_MONTH);

		String strYear = "" + thisYear;
		String strMonth = "" + thisMonth;
		if (strMonth.length() == 1) {
			strMonth = "0" + strMonth;
		}
		String strDay = "" + thisDay;
		if (strDay.length() == 1) {
			strDay = "0" + strDay;
		}

		String dateStr = strYear + strMonth + strDay;

		return dateStr;
	}

	/**
	 * 转换时间格式
	 * 
	 * @param dateStr
	 * @return
	 */
	@SuppressLint("SimpleDateFormat")
	public static String formatDate(String dateStr) {
		String dateResultString = "";
		SimpleDateFormat myFormatter = new SimpleDateFormat("yyyyMMdd");
		try {
			Date date = myFormatter.parse(dateStr);

			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			dateResultString = formatter.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return dateResultString;
	}

	/**
	 * 转换时间格式
	 * 
	 * @param dateStr
	 * @return
	 */
	@SuppressLint("SimpleDateFormat")
	public static String formatDateTwo(String dateStr) {
		String dateResultString = "";
		SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Date date = myFormatter.parse(dateStr);

			SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
			dateResultString = formatter.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return dateResultString;
	}
	
	/**
	 * 获取指定月份相差differ的月份
	 * @param specificMonth
	 * @param differ
	 * @return
	 */
	public static String getDifferMonth(String specificMonth, int differ) {
		
		int year = Integer.valueOf(specificMonth.substring(0, 4));
		int month = Integer.valueOf(specificMonth.substring(4, 6));
		
		Calendar thisCalendar = Calendar.getInstance();
		thisCalendar.set(Calendar.YEAR, year);
		thisCalendar.set(Calendar.MONTH, month - 1);
		thisCalendar.add(Calendar.MONTH, differ);
		int thisYear = thisCalendar.get(Calendar.YEAR);
		int thisMonth = thisCalendar.get(Calendar.MONTH) + 1;
		
		String strMonth = "" + thisMonth;
		if (strMonth.length() == 1) {
			strMonth = "0" + strMonth;
		}
		
		return "" + thisYear + strMonth;
	}
	
	/**
	 * 获取当前周的周一日期
	 * @return
	 */
	public static String getThisMondayStr() {

		// 当前时间对象
		Calendar thisCalendar = getCurrentMonday();
		// 当前年
		int thisYear = thisCalendar.get(Calendar.YEAR);
		// 当前月(获取月份得加1)
		int thisMonth = thisCalendar.get(Calendar.MONTH) + 1;
		// 当前日
		int thisDay = thisCalendar.get(Calendar.DAY_OF_MONTH);

		String strYear = "" + thisYear;
		String strMonth = "" + thisMonth;
		if (strMonth.length() == 1) {
			strMonth = "0" + strMonth;
		}
		String strDay = "" + thisDay;
		if (strDay.length() == 1) {
			strDay = "0" + strDay;
		}

		String dateStr = strYear + strMonth + strDay;

		return dateStr;
	}
	
	//（1）获得当前日期与本周一相差的天数
    private static int getMondayPlus() {
        Calendar cd = Calendar.getInstance();
        // 获得今天是一周的第几天，星期日是第一天，星期二是第二天......
        int dayOfWeek = cd.get(Calendar.DAY_OF_WEEK);
        if (dayOfWeek == 1) {
            return -6;
        } else {
            return 2 - dayOfWeek;
        }
    }


    //（2） 获得本周星期一的日期
    public static Calendar getCurrentMonday() {
        int mondayPlus = getMondayPlus();
        GregorianCalendar currentDate = new GregorianCalendar();
        currentDate.add(GregorianCalendar.DATE, mondayPlus);
        Date monday = currentDate.getTime();
        Calendar c = Calendar.getInstance();
        c.setTime(monday);
        return c;
    }
    
    /**
     * 
     * @param month
     * @return
     */
    public static int getMonthAbbr(String month) {
    	int monthId = 0;
    	if ("01".equals(month)) {
    		monthId = R.string.month_1;
    	} else if ("02".equals(month)) {
    		monthId = R.string.month_2;
    	} else if ("03".equals(month)) {
    		monthId = R.string.month_3;
    	} else if ("04".equals(month)) {
    		monthId = R.string.month_4;
    	} else if ("05".equals(month)) {
    		monthId = R.string.month_5;;
    	} else if ("06".equals(month)) {
    		monthId = R.string.month_6;
    	} else if ("07".equals(month)) {
    		monthId = R.string.month_7;
    	} else if ("08".equals(month)) {
    		monthId = R.string.month_8;
    	} else if ("09".equals(month)) {
    		monthId = R.string.month_9;
    	} else if ("10".equals(month)) {
    		monthId = R.string.month_10;
    	} else if ("11".equals(month)) {
    		monthId = R.string.month_11;
    	} else if ("12".equals(month)) {
    		monthId = R.string.month_12;
    	}
    	
    	return monthId;
    }
}

