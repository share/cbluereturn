package com.until;

import java.util.Locale;

import android.util.Log;

/**
 * Convert between integer and byte or array of them.
 * <P>
 * Here, when get integer from byte more than two bytes, leave the high bit as
 * sign note.
 * 
 * @author duanfs
 * @since 2013.10.11
 * @version 1.0 The data part of protocol, also contains the CRC verification
 *          operation class.
 * @version 1.2 Change it to method class operate with byte and integer.
 * @version 1.4 Construct a new inner class of high byte after operation.
 * @version 1.6 Add find method.
 */
public class Dat {
	private static final String TAG = "Dat";
	private static final boolean D = !true;
	private static final boolean highBitSign = false;

	/**
	 * All the method depend the normal byte(s), but the high byte in the end.
	 * 
	 * @author duanfs
	 * @since 2013.10.31
	 * @version 1.0
	 */
	public static final class DatHA {
		/**
		 * Get the value of integer from byte array at the proper position of
		 * length less than 4 bytes.
		 * 
		 * @param bytes
		 *            byte array contains value.
		 * @param offset
		 *            the start index.
		 * @param count
		 *            the count to convert to integer {@code}count<=4 and
		 *            {@code} count>=1 if {@code}count>4 or less than 1, trim it
		 *            to 4.
		 * @return the value of integer. -1 for bytes not long enough.
		 * @throws IndexOutOfBoundsException
		 *             if {@code offset < 0} or {@code count < 0}, or if
		 *             {@code offset + count} is bigger than the length of
		 *             {@code buffer}.
		 */
		public static final int getInt(byte[] bytes, int offset, int count) {
			try {
				Util.checkBoundsofArray(bytes.length, offset, count);
			} catch (IndexOutOfBoundsException e) {
				return -1;
			}
			switch (count) {
			case 1:
				return oneByte(bytes[offset]);
			case 2:
				return twoByte(bytes[offset], bytes[offset + 1]);
			case 3:
				return threeByte(bytes[offset], bytes[offset + 1],
						bytes[offset + 2]);
			case 4:
				return toInt(bytes[offset], bytes[offset + 1],
						bytes[offset + 2], bytes[offset + 3]);
			default:
				if (count > 4)
					return toInt(bytes[offset], bytes[offset + 1],
							bytes[offset + 2], bytes[offset + 3]);
				else
					throw new IllegalArgumentException("count= " + count);
			}
		}

		/**
		 * Get the array values of int type from byte array
		 * <P>
		 * The high byte in back
		 * 
		 * @param bytes
		 *            contains the original value
		 * @param type
		 *            the length of Byte to a number
		 * @type integer less than 4 and bigger than 0
		 * @return the array of number
		 */
		public static final int[] toNumber(byte[] bytes, int type) {
			// Used to get byte
			final int[] MAX = { 0xff000000, 0x00ff0000, 0x0000ff00, 0x000000ff };
			// The byte number of integer `
			int LEN = Integer.SIZE / Byte.SIZE;
			// Bigger than integer, use integer
			if (type > LEN || type < 1)
				type = (byte) LEN;
			// Cut off last byte less than integer byte number
			int length = bytes.length - bytes.length % type;
			// The return integer array
			int[] array = new int[length / type];
			int temp = 0, l = 0, t = 0;
			for (int i = 0; i < length / type; i++) {
				temp = 0;
				// There remain the sign of integer same as the highest byte
				l = (bytes[i * type + 0] << ((type - 1) * Byte.SIZE)) & 0xffffffff;
				temp |= l;
				for (int j = 0; j < type; j++) {
					// Shift byte to proper position and cut off minus
					t = type - j - 1;
					l = (bytes[i * type + t] << (t * Byte.SIZE))
							& MAX[LEN - type + j];
					// Put all byte in the proper position
					temp |= l;
				}
				array[i] = temp;
			}
			return array;
		}

		/**
		 * Get the byte array of {@code}value restricted to {@code} type
		 * <P>
		 * The high byte in back
		 * 
		 * @param value
		 *            the integer
		 * @param type
		 *            the length of Byte to a number
		 * @type integer less than 4 and bigger than 0
		 * @return the byte array of integer
		 */
		public static final byte[] toByte(int value, int type) {
			int LEN = Integer.SIZE / Byte.SIZE;
			// if (type == -1) {
			// if (Byte.MAX_VALUE >= value || Byte.MIN_VALUE <= value)
			// type = 1;
			// else if (Short.MAX_VALUE >= value || Short.MIN_VALUE <= value)
			// type = 2;
			// else if (Integer.MAX_VALUE >= value || Integer.MIN_VALUE <=
			// value)
			// type = 4;
			// } else {
			// if ((type > LEN || type < 0))
			// type = (byte) LEN;
			// }
			if (type > LEN || type <= 0)
				return null;
			byte[] ab = new byte[type];
			for (int j = 0; j < type; j++) {
				// Get the j+1 position byte of value
				ab[type - j - 1] = Dat.toByte(value, j + 1, type);
			}
			return ab;
		}

		/**
		 * 
		 * Converts a subsequence of the byte array to a string using Hex
		 * chars,which means one byte can be converted to two char of Hex.
		 * <P>
		 * The high byte in back
		 */
		public static final String toHexString(byte[] bytes, int offset,
				int count) throws IndexOutOfBoundsException,
				NullPointerException {
			Util.checkBoundsofArray(bytes.length, offset, count);
			StringBuffer sb = new StringBuffer(2 * count);
			for (int i = 0; i < count; i++) {
				sb.append(Integer.toHexString(bytes[i] & 0x0000000f)
						.toUpperCase(new Locale("en")));
				sb.append(Integer.toHexString((bytes[i] & 0x000000f0) >> 4)
						.toUpperCase(new Locale("en")));
			}
			return sb.toString();
		}

		/**
		 * 
		 * Converts a subsequence of the byte array to a byte array by cutting
		 * half. The high byte in back
		 * 
		 * @param bytes
		 *            the data to be cut half.
		 * @param offset
		 *            the start position in {@code bytes} from where to get
		 *            bytes.
		 * @param count
		 *            the number of bytes from {@code bytes} to write to this
		 *            data.
		 * @return the string represent of {@code bytes} * @throws
		 *         NullPointerException if {@code bytes == null}.
		 * @throws IndexOutOfBoundsException
		 *             if
		 *             {@code byteCount < 0 || offset < 0 || offset + byteCount > bytes.length}
		 *             .
		 */
		public static final byte[] halfByte(byte[] bytes, int offset, int count) {
			Util.checkBoundsofArray(bytes.length, offset, count);
			byte[] bya = new byte[count * 2];
			for (int i = 0; i < count; i++) {
				bya[i * 2] = (byte) Dat.getHalfByte(bytes[i], false);
				bya[i * 2 + 1] = (byte) Dat.getHalfByte(bytes[i], true);
			}
			return bya;
		}

		/**
		 * Get the value of integer from byte array at the proper position of
		 * length less than 4 bytes. The high byte at back.
		 * 
		 * @param bytes
		 *            byte array contains value.
		 * @param offset
		 *            the start index.
		 * @param count
		 *            the count to convert to integer {@code}count<=4 and
		 *            {@code} count>=1 if {@code}count>4 or less than 1, trim it
		 *            to 4.
		 * @param value
		 *            the value to find.
		 * @return the first index that equal to value.-1 for not found.
		 * @throws IndexOutOfBoundsException
		 *             if {@code offset < 0} or {@code count < 0}, or if
		 *             {@code offset + count} is bigger than the length of
		 *             {@code buffer}.
		 */
		public static final int find(byte[] bytes, int offset, int count,
				int value) {
			Util.checkBoundsofArray(bytes.length, offset, count);
			int num = (bytes.length - offset) / count;
			for (int i = 0; i < num; i++)
				if (Dat.DatHA.getInt(bytes, offset + count * i, count) == value)
					return offset + count * i;
			return -1;
		}

		/**
		 * Convert two byte to short.
		 * <P>
		 * The high byte in back
		 * 
		 */
		public static int twoByte(byte high, byte low) {
			int l = (low << Byte.SIZE) & 0xffffffff;
			l |= high & 0x00ff;
			if (D)
				Log.d(TAG + "$HA", "" + l + "/" + Integer.toBinaryString(l));
			return l;
		}

		/**
		 * Convert two byte to short.
		 * <P>
		 * The high byte in back
		 * 
		 */
		public static int twoByte(int high, int low) {
			int l = (low << Byte.SIZE) & 0xffffffff;
			l |= high & 0x00ff;
			if (D)
				Log.d(TAG + "$HA", "" + l + "/" + Integer.toBinaryString(l));
			return l;
		}

		/**
		 * Convert three byte to short.
		 * <P>
		 * The high byte in back
		 * 
		 */
		public static final int threeByte(byte high, byte middle, byte low) {
			int l = (low << Byte.SIZE * 2) & 0xffffffff;
			l |= (middle << Byte.SIZE) & 0x00ff00;
			l |= high & 0x00ff;
			if (D)
				Log.d(TAG + "$HA", "" + l + "/" + Integer.toBinaryString(l));
			return l;
		}

		/**
		 * Convert four byte to short.
		 * <P>
		 * The high byte in back
		 * 
		 */
		public static final int toInt(byte high, byte middle, byte low,
				byte lowest) {
			int l = (lowest << Byte.SIZE * 3) & 0xffffffff;
			l |= (low << Byte.SIZE * 2) & 0x00ff0000;
			l |= (middle << Byte.SIZE) & 0x00ff00;
			l |= high & 0x00ff;
			if (D)
				Log.d(TAG + "$HA", "" + l + "/" + Integer.toBinaryString(l));
			return l;
		}

		public static int toInt(int high, int middle, int low, int lowest) {
			int l = (lowest << Byte.SIZE * 3) & 0xffffffff;
			l |= (low << Byte.SIZE * 2) & 0x00ff0000;
			l |= (middle << Byte.SIZE) & 0x00ff00;
			l |= high & 0x00ff;
			if (D)
				Log.d(TAG + "$HA", "" + l + "/" + Integer.toBinaryString(l));
			return l;
		}

	}

	/**
	 * Get the byte array of current data *
	 * 
	 * @return
	 */

	/**
	 * Get the array values of int type from byte array
	 * <P>
	 * The high byte in front
	 * 
	 * @param bytes
	 *            contains the original value
	 * @param type
	 *            the length of Byte to a number
	 * @type integer less than 4 and bigger than 0
	 * @return the array of number
	 */
	public static final int[] toNumber(byte[] bytes, int type) {
		// Used to get byte
		final int[] MAX = { 0xff000000, 0x00ff0000, 0x0000ff00, 0x000000ff };
		// The byte number of integer `
		int LEN = Integer.SIZE / Byte.SIZE;
		// Bigger than integer, use integer
		if (type > LEN || type < 1)
			type = (byte) LEN;
		// Cut off last byte less than integer byte number
		int length = bytes.length - bytes.length % type;
		// The return integer array
		int[] array = new int[length / type];
		int temp = 0, l = 0;
		for (int i = 0; i < length / type; i++) {
			temp = 0;
			// There remain the sign of integer same as the highest byte
			l = (bytes[i * type + 0] << ((type - 1) * Byte.SIZE)) & 0xffffffff;
			temp |= l;
			for (int j = 1; j < type; j++) {
				// Shift byte to proper position and cut off minus
				l = (bytes[i * type + j] << ((type - j - 1) * Byte.SIZE))
						& MAX[LEN - type + j];
				// Put all byte in the proper position
				temp |= l;
			}
			array[i] = temp;
		}
		return array;
	}

	/**
	 * Get the byte array of {@code}value restricted to {@code} type
	 * <P>
	 * The high byte in front
	 * 
	 * @param value
	 *            the integer
	 * @param type
	 *            the length of Byte to a number. Integer less than 4 and bigger
	 *            than 0
	 * @return the byte array of integer. Null for illegealArgument.
	 */
	public static final byte[] toByte(int value, int type) {
		// Log.d(TAG, "toByte" + value + "/" + type);
		int LEN = Integer.SIZE / Byte.SIZE;
		// if (type == -1) {
		// if (Byte.MAX_VALUE >= value || Byte.MIN_VALUE <= value)
		// type = 1;
		// else if (Short.MAX_VALUE >= value || Short.MIN_VALUE <= value)
		// type = 2;
		// else if (Integer.MAX_VALUE >= value || Integer.MIN_VALUE <= value)
		// type = 4;
		// } else {
		// if ((type > LEN || type < 0))
		// type = (byte) LEN;
		// }
		if (type > LEN || type < 0)
			return null;
		byte[] ab = new byte[type];
		for (int j = 0; j < type; j++) {
			// Get the j+1 position byte of value
			ab[j] = toByte(value, j + 1, type);
		}
		return ab;
	}

	/**
	 * Get the byte at {@code}position in total bytes count {@code}type of
	 * integer {@code}value
	 * 
	 * @param value
	 *            the integer
	 * @param position
	 *            the place in total byte count .The value may be 1...type
	 * @param type
	 *            the byte count for {@code}value
	 * @return the byte value
	 */
	public static final byte toByte(int value, int position, int type) {
		int MAX = 0x000000ff;
		return (byte) ((value >> ((type - position) * Byte.SIZE)) & MAX);
	}

	/**
	 * Get short of half byte.
	 * 
	 * @param value
	 *            the byte value
	 * @param high
	 *            true for high half byte or low half
	 * @return short value
	 */
	public static final short getHalfByte(byte value, boolean high) {
		if (high)
			return (short) ((value & 0x000000f0) >> 4);
		else
			return (short) (value & 0x0000000f);
	}

	/**
	 * Get short of high byte.
	 * 
	 * @param value
	 *            the byte value
	 * @param high
	 *            true for high byte or low
	 * @return short value
	 */
	public static final short getHighByte(byte value, boolean high) {
		if (high)
			return (short) (value & 0x000000f0);
		else
			return (short) (value & 0x0000000f);
	}

	/**
	 * Converts a subsequence of the byte array to a char array.
	 * <P>
	 * One byte represents one char.
	 * 
	 * @param bytes
	 *            the data to be converted.
	 * @param offset
	 *            the start position in {@code bytes} from where to get bytes.
	 * @param count
	 *            the number of bytes from {@code bytes} to write to this data.
	 * @return the string represent of {@code bytes}
	 * @throws NullPointerException
	 *             if {@code bytes == null}.
	 * @throws IndexOutOfBoundsException
	 *             if
	 *             {@code byteCount < 0 || offset < 0 || offset + byteCount > bytes.length}
	 *             .
	 */
	public static char[] toChar(byte[] bytes, int offset, int count)
			throws IndexOutOfBoundsException, NullPointerException {
		Util.checkBoundsofArray(bytes.length, offset, count);
		char[] chars = new char[count];
		for (int i = 0; i < count; i++)
			chars[i] = (char) bytes[offset + i];
		return chars;
	}

	/**
	 * Converts a subsequence of the byte array to a char array.
	 * <P>
	 * Two bytes represent one char.
	 * 
	 * @param bytes
	 *            the data to be converted.
	 * @param offset
	 *            the start position in {@code bytes} from where to get bytes.
	 * @param count
	 *            the number of bytes from {@code bytes} to write to this data.
	 * @return the string represent of {@code bytes}
	 * @throws NullPointerException
	 *             if {@code bytes == null}.
	 * @throws IndexOutOfBoundsException
	 *             if
	 *             {@code byteCount < 0 || offset < 0 || offset + byteCount > bytes.length}
	 *             .
	 */
	public static char[] toChar2(byte[] bytes, int offset, int count)
			throws IndexOutOfBoundsException, NullPointerException {
		Util.checkBoundsofArray(bytes.length, offset, count);
		char[] chars = new char[count / 2];
		for (int i = 0; i < count / 2;) {
			chars[i] = (char) twoByte(bytes[i], bytes[i + 1]);
			i += 2;
		}
		return chars;
	}

	/**
	 * 
	 * Converts a subsequence of the byte array to a string using Hex
	 * chars,which means one byte can be converted to two char of Hex.
	 * 
	 * @param bytes
	 *            the data to be converted.
	 * @param offset
	 *            the start position in {@code bytes} from where to get bytes.
	 * @param count
	 *            the number of bytes from {@code bytes} to write to this data.
	 * @return the string represent of {@code bytes}
	 * @throws NullPointerException
	 *             if {@code bytes == null}.
	 * @throws IndexOutOfBoundsException
	 *             if
	 *             {@code byteCount < 0 || offset < 0 || offset + byteCount > bytes.length}
	 *             .
	 */
	public static final String toHexString(byte[] bytes, int offset, int count)
			throws IndexOutOfBoundsException, NullPointerException {
		Util.checkBoundsofArray(bytes.length, offset, count);
		StringBuffer sb = new StringBuffer(2 * count);
		for (int i = 0; i < count; i++) {
			sb.append(Integer.toHexString((bytes[i] & 0x000000f0) >> 4)
					.toUpperCase(new Locale("en")));
			sb.append(Integer.toHexString(bytes[i] & 0x0000000f).toUpperCase(
					new Locale("en")));
		}
		return sb.toString();
	}

	/**
	 * 
	 * Converts a subsequence of the byte array to a byte array by cutting half.
	 * The high byte in front.
	 * 
	 * @param bytes
	 *            the data to be cut half.
	 * @param offset
	 *            the start position in {@code bytes} from where to get bytes.
	 * @param count
	 *            the number of bytes from {@code bytes} to write to this data.
	 * @return the string represent of {@code bytes} * @throws
	 *         NullPointerException if {@code bytes == null}.
	 * @throws IndexOutOfBoundsException
	 *             if
	 *             {@code byteCount < 0 || offset < 0 || offset + byteCount > bytes.length}
	 *             .
	 */
	public static final byte[] halfByte(byte[] bytes, int offset, int count) {
		Util.checkBoundsofArray(bytes.length, offset, count);
		byte[] bya = new byte[count * 2];
		for (int i = 0; i < count; i++) {
			bya[i * 2] = (byte) Dat.getHalfByte(bytes[i], true);
			bya[i * 2 + 1] = (byte) Dat.getHalfByte(bytes[i], false);
		}
		return bya;
	}

	/**
	 * Get the value of integer from byte array at the proper position of length
	 * less than 4 bytes.
	 * 
	 * @param bytes
	 *            byte array contains value.
	 * @param offset
	 *            the start index.
	 * @param count
	 *            the count to convert to integer {@code}count<=4 and {@code}
	 *            count>=1 if {@code}count>4 or less than 1, trim it to 4.
	 * @param value
	 *            the value to find.
	 * @return the first index that equal to value.-1 for not found.
	 * @throws IndexOutOfBoundsException
	 *             if {@code offset < 0} or {@code count < 0}, or if
	 *             {@code offset + count} is bigger than the length of
	 *             {@code buffer}.
	 */
	public static final int find(byte[] bytes, int offset, int count, int value) {
		Util.checkBoundsofArray(bytes.length, offset, count);
		int num = (bytes.length - offset) / count;
		for (int i = 0; i < num; i++)
			if (Dat.getInt(bytes, offset + count * i, count) == value)
				return offset + count * i;
		return -1;
	}

	/**
	 * Get the value of integer from byte array at the proper position of length
	 * less than 4 bytes.
	 * 
	 * @param bytes
	 *            byte array contains value.
	 * @param offset
	 *            the start index.
	 * @param count
	 *            the count to convert to integer {@code}count<=4 and {@code}
	 *            count>=1 if {@code}count>4 or less than 1, trim it to 4.
	 * @return the value of integer. -1 for bytes not long enough.
	 * @throws IndexOutOfBoundsException
	 *             if {@code offset < 0} or {@code count < 0}, or if
	 *             {@code offset + count} is bigger than the length of
	 *             {@code buffer}.
	 */
	public static final int getInt(byte[] bytes, int offset, int count) {
		try {
			Util.checkBoundsofArray(bytes.length, offset, count);
		} catch (IndexOutOfBoundsException e) {
			return -1;
		}
		switch (count) {
		case 1:
			return oneByte(bytes[offset]);
		case 2:
			return twoByte(bytes[offset], bytes[offset + 1]);
		case 3:
			return threeByte(bytes[offset], bytes[offset + 1],
					bytes[offset + 2]);
		case 4:
			return toInt(bytes[offset], bytes[offset + 1], bytes[offset + 2],
					bytes[offset + 3]);
		default:
			if (count > 4)
				return toInt(bytes[offset], bytes[offset + 1],
						bytes[offset + 2], bytes[offset + 3]);
			else
				throw new IllegalArgumentException("count= " + count);
		}
	}

	/**
	 * Convert one byte to short
	 * 
	 * @param bit
	 *            the byte to convert
	 * @return integer value of byte
	 */
	public static final int oneByte(byte bit) {
		int l = bit & 0xff;
		if (D)
			Log.d(TAG, "" + l + "/" + Integer.toBinaryString(l));
		return l;
	}

	public static int twoByte(byte high, byte low) {
		int l = 0;
		if (highBitSign)
			l = high << Byte.SIZE;
		else
			l = (high << Byte.SIZE) & 0x0000ffff;
		l |= low & 0x00ff;
		if (D)
			Log.d(TAG, "" + l + "/" + Integer.toBinaryString(l));
		return l;
	}

	public static final int threeByte(byte high, byte middle, byte low) {
		int l = 0;
		if (highBitSign)
			l = (high << Byte.SIZE * 2); // & 0xffffffff;
		else
			l = (high << Byte.SIZE * 2) & 0x00ffffff;
		l |= (middle << Byte.SIZE) & 0x00ff00;
		l |= low & 0x00ff;
		if (D)
			Log.d(TAG, "" + l + "/" + Integer.toBinaryString(l));
		return l;
	}

	public static final int toInt(byte high, byte middle, byte low, byte lowest) {
		int l = (high << Byte.SIZE * 3); // & 0xffffffff;
		l |= (middle << Byte.SIZE * 2) & 0x00ff0000;
		l |= (low << Byte.SIZE) & 0x00ff00;
		l |= lowest & 0x00ff;
		if (D)
			Log.d(TAG, "" + l + "/" + Integer.toBinaryString(l));
		return l;
	}

	/**
	 * Get the sum value of integer from byte array.
	 * 
	 * @param bytes
	 *            byte array contains value.
	 * @param offset
	 *            the start index.
	 * @param count
	 *            the number of bytes from {@code bytes} to sum.
	 * @return the value .
	 * @throws IndexOutOfBoundsException
	 *             if {@code offset < 0} or {@code count < 0}, or if
	 *             {@code offset + count} is bigger than the length of
	 *             {@code buffer}.
	 */
	public static final int sum(byte[] bytes, int offset, int count) {
		Util.checkBoundsofArray(bytes.length, offset, count);
		int l = 0;
		for (int i = 0; i < count; i++) {
			l += oneByte(bytes[offset + i]);
		}
		return l;
	}

	/**
	 * Get the sum value of integer from int array.
	 * 
	 * @param bytes
	 *            byte array contains value.
	 * @param offset
	 *            the start index.
	 * @param count
	 *            the number of bytes from {@code bytes} to sum.
	 * @return the value .
	 * @throws IndexOutOfBoundsException
	 *             if {@code offset < 0} or {@code count < 0}, or if
	 *             {@code offset + count} is bigger than the length of
	 *             {@code buffer}.
	 */
	public static final int sum(int[] bytes, int offset, int count) {
		Util.checkBoundsofArray(bytes.length, offset, count);
		int l = 0;
		for (int i = 0; i < count; i++) {
			l += bytes[offset + i];
			// Log.d(TAG,i+ ""+bytes[offset + i]);
		}
		return l;
	}

	/**
	 * Get the binary array of one byte integer.
	 * 
	 * @param oneByte
	 *            the integer.
	 * @return a byte array.
	 */
	public static byte[] getBinary(int oneByte) {
		byte[] bins = new byte[8];
		int indicator = 0x1;
		for (int i = 0; i < 8; i++) {
			bins[7 - i] = (byte) ((0 == (oneByte & (indicator << i))) ? 0 : 1);
		}
		return bins;
	}

	/**
	 * This class is used to calculate byte CRC value.
	 * <P>
	 * G=X^8+X^5+X^4+1 where ^ is "not same true" is the formation to calculate
	 * CRC.
	 * 
	 * 
	 * @author duanfs
	 * @version 1.0
	 * @version 1.2
	 *          {@code deprecated methods getCRC_8(short base, byte[] bytes) and verify(byte[] bytes)}
	 * @version 1.4
	 *          <P>
	 *          {@code Create method getCRC_8(int base, int next) to get CRC of two CRCs. Create method
	 * getCRC_8(byte[] bytes, int offset, int count, int next) to get CRC of next based on byte array.}
	 *          2013.11.14
	 * @version 1.41
	 *          <P>
	 *          {@code Create method get getCRC_8_index(int old, byte[] bytes, int offset, int count) 
	 *          to get CRC index. Modify  getCRC_8(int base, int next) as getCRC_8(int base, int nextIndex)
	 *          and getCRC_8(byte[] bytes, int offset, int count, int next) as getCRC_8(byte[] bytes, int offset,
	 *           int count, int nextIndex)}. 2013.11.15
	 * @version 2.0
	 *          <P>
	 *          {@code Remove methods added or modified in version1.4 and 1.41}.
	 *          <P>
	 *          {@code getCRC_8(0,bytes,offset,count)��  getCRC_8( getCRC_8(0,}
	 *          {@code bytes,offset,count-2),getCRC_8(0,bytes,offset+count-2,2)}
	 *          {@code or getCRC_8_index(0,bytes,offset+count-2,2) )}
	 * @version 2.01 reuse method getCRC_8(short base, byte[] bytes),but overrde
	 *          it 2013.11.26.
	 * 
	 */
	public final static class CRC {
		/**
		 * CRC formation contains character *^/|&()-+
		 */
		// private static String FORMATION = "x^8+x^5+x^4+1";

		/**
		 * Set the formation to calculate CRC
		 * 
		 * @param formation
		 */
		// public static final void setCalculateFormation(final String
		// formation) {
		// FORMATION = formation;
		// }

		/**
		 * Need considering
		 * 
		 * @param bit
		 * @return
		 */
		@Deprecated
		public static int getCRC_(int bit) {
			// int i = FORMATION.hashCode();
			return -1;
		}

		/**
		 * verification table
		 */
		private final static short crc_table[] = { 0, 94, 188, 226, 97, 63,
				221, 131, 194, 156, 126, 32, 163, 253, 31, 65, 157, 195, 33,
				127, 252, 162, 64, 30, 95, 1, 227, 189, 62, 96, 130, 220, 35,
				125, 159, 193, 66, 28, 254, 160, 225, 191, 93, 3, 128, 222, 60,
				98, 190, 224, 2, 92, 223, 129, 99, 61, 124, 34, 192, 158, 29,
				67, 161, 255, 70, 24, 250, 164, 39, 121, 155, 197, 132, 218,
				56, 102, 229, 187, 89, 7, 219, 133, 103, 57, 186, 228, 6, 88,
				25, 71, 165, 251, 120, 38, 196, 154, 101, 59, 217, 135, 4, 90,
				184, 230, 167, 249, 27, 69, 198, 152, 122, 36, 248, 166, 68,
				26, 153, 199, 37, 123, 58, 100, 134, 216, 91, 5, 231, 185, 140,
				210, 48, 110, 237, 179, 81, 15, 78, 16, 242, 172, 47, 113, 147,
				205, 17, 79, 173, 243, 112, 46, 204, 146, 211, 141, 111, 49,
				178, 236, 14, 80, 175, 241, 19, 77, 206, 144, 114, 44, 109, 51,
				209, 143, 12, 82, 176, 238, 50, 108, 142, 208, 83, 13, 239,
				177, 240, 174, 76, 18, 145, 207, 45, 115, 202, 148, 118, 40,
				171, 245, 23, 73, 8, 86, 180, 234, 105, 55, 213, 139, 87, 9,
				235, 181, 54, 104, 138, 212, 149, 203, 41, 119, 244, 170, 72,
				22, 233, 183, 85, 11, 136, 214, 52, 106, 43, 117, 151, 201, 74,
				20, 246, 168, 116, 42, 200, 150, 21, 75, 169, 247, 182, 232,
				10, 84, 215, 137, 107, 53 };

		/**
		 * Get the CRC value according to the index
		 * 
		 * @param index
		 *            the index in the CRC table
		 * @return the value of CRC. -1 for out of bounds.
		 */
		public static short getCRC_8(int index) {
			// bigger than the max value of table
			if (index > 255 || index < 0)
				// the smaller value of index at last 8 bits
				return -1;
			else
				// normal value of index
				return crc_table[index];
		}

		/**
		 * Get the CRC value of bytes
		 * 
		 * @param bytes
		 *            the byte array
		 * @return the value of CRC
		 */
		public static int getCRC_8(byte[] bytes) {
			int crc_old = 0;
			int crc_new = 0;
			for (int i = 0; i < bytes.length; i++) {
				// get the value of last 8 bits
				crc_new = crc_table[(crc_old ^ bytes[i]) & 0x00FF];
				crc_old = crc_new;
			}
			return crc_new;
		}

		/**
		 * Get the CRC value of bytes start at {@code base}
		 * 
		 * @param base
		 *            the CRC value to start
		 * @param bytes
		 *            the byte array
		 * @return the value of CRC
		 */
		// @Deprecated
		public static int getCRC_8(int base, byte[] bytes) {
			return getCRC_8(base, bytes, 0, bytes.length);
		}

		/**
		 * Get the CRC value of bytes based on given value start from
		 * {@code offset} to {@code offset+count}
		 * 
		 * @param base
		 *            the CRC value to start
		 * @param bytes
		 *            the byte array
		 * @param offset
		 *            the start position to check
		 * @param count
		 *            the count to check
		 * 
		 * @throws if
		 *             {@code offset < 0} or {@code count < 0}, or if
		 *             {@code offset + count} is bigger than the length of
		 *             {@code bytes}.
		 * @throws NullPointerException
		 *             if {@code bytes is null}
		 */
		public static int getCRC_8(int base, byte[] bytes, int offset, int count)
				throws IndexOutOfBoundsException {
			if (bytes == null) {
				if (D)
					Log.d(TAG, "Byte array is");
				return base;
			}
			// Util.checkBoundsofArray(bytes.length, offset, count);
			int crc_old = base;
			int crc_new = base;
			for (int i = 0; i < count; i++) {
				// get the value of last 8 bits
				crc_new = crc_table[(crc_old ^ bytes[i + offset]) & 0x00FF];
				crc_old = crc_new;
			}
			return crc_new;
		}

		/**
		 * Check the verification of bytes to the given CRC value
		 * 
		 * @param bytes
		 *            the byte array
		 * @param value
		 *            the given CRC value
		 * @return true verification okay, or err
		 */
		public static boolean verify(byte[] bytes, int value) {
			return getCRC_8(0, bytes, 0, bytes.length) == value;
		}

		/**
		 * Check the verification of bytes start from {@code offset} to
		 * {@code offset+count} to the given CRC value
		 * 
		 * @param bytes
		 *            the byte array
		 * @param offset
		 *            the start position to check
		 * @param count
		 *            the count to check
		 * @param value
		 *            the given value to compare
		 * @return true if equal to {@code}value or false
		 * @throws if
		 *             {@code offset < 0} or {@code count < 0}, or if
		 *             {@code offset + count} is bigger than the length of
		 *             {@code bytes}.
		 * @throws NullPointerException
		 *             if {@code bytes is null}
		 */
		public static boolean verify(byte[] bytes, int offset, int count,
				int value) throws IndexOutOfBoundsException {
			return value == getCRC_8(0, bytes, offset, count);
		}

		/**
		 * Check the verification of bytes,the last byte is CRC
		 * 
		 * @param bytes
		 *            the byte array
		 * @return true verification okay, or err
		 */
		@Deprecated
		public static boolean verify(byte[] bytes) {
			byte[] temp = new byte[bytes.length - 1];
			for (int i = 0; i < bytes.length - 1; i++) {
				temp[i] = bytes[i];
			}
			return verify(temp, bytes[bytes.length - 1]);
		}

		/**
		 * Get the CRC value of a byte
		 * 
		 * @param b
		 *            the byte
		 * @return the value of CRC
		 */
		public static int getCRC_8(byte b) {
			return crc_table[b & 0x00FF];
		}
	}

	public static final int toInt(int high, int middle, int low, int lowest) {
		int l = (high << Byte.SIZE * 3); // & 0xffffffff;
		l |= (middle << Byte.SIZE * 2) & 0x00ff0000;
		l |= (low << Byte.SIZE) & 0x00ff00;
		l |= lowest & 0x00ff;
		if (D)
			Log.d(TAG, "" + l + "/" + Integer.toBinaryString(l));
		return l;
	}
}
