package com.until;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;

public class HttpUtils {

	/** 上传数据到服务器, 接收返回信息 */
	public static String httpPostData(String urlString,
			List<NameValuePair> nameValuePairs) {

		String result = "";

		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(urlString);
		StringBuilder builder = new StringBuilder();
		String line = null;
		try {
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs,
					HTTP.UTF_8));
			HttpResponse response = httpclient.execute(httppost);
			int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode == 200) {
				HttpEntity entity = response.getEntity();
				InputStream inputStream = entity.getContent();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(inputStream));
				while ((line = reader.readLine()) != null) {
					builder.append(line);
				}
				result = builder.toString();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;

	}

	/**
	 * ����������ӿ�
	 * 
	 * @param url
	 * @param nameValuePairs
	 * @return response
	 */
	public static String httpRequestForServer(String url,
			List<NameValuePair> nameValuePairs) {
		String response = null;
		QHttpClient http = new QHttpClient();
		try {
			response = http.httpPost(url, nameValuePairs);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return "request error !";
		}
		return response;
	}

	/**
	 * ����������ӿ�
	 * 
	 * @param url
	 * @param nameValuePairs
	 * @return response
	 */
	public static String httpRequestForServerByGet(String url,
			HashMap<String, String> map) {
		String response = null;
		QHttpClient http = new QHttpClient();
		try {
			response = http.httpGet(url, map);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return "request error !";
		}
		return response;
	}
}
