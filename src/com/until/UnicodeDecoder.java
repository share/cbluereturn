package com.until;

public class UnicodeDecoder {

	public static void main(String[] args) {
		String s = "\u5317\u4eac\u5e02";
		System.out.println(decode(s));
	}

	public static String decode(String in) {
		try {
			return decode(in.toCharArray());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return in;
	}

	private static String decode(char[] in) throws Exception {
		int off = 0;
		char c;
		char[] out = new char[in.length];
		int outLen = 0;
		while (off < in.length) {
			c = in[off++];
			if (c == '\\') {
				if (in.length > off) { // 閺勵垰鎯侀張澶夌瑓娑擄拷閲滅�妤冾儊
					c = in[off++]; // 閸欐牕鍤稉瀣╃娑擃亜鐡х粭锟�
				} else {
					out[outLen++] = '\\'; // 閺堫偄鐡х粭锔胯礋'\'閿涘矁绻戦崶锟�
					break;
				}
				if (c == 'u') {
					int value = 0;
					if (in.length > off + 4) { // 閸掋倖鏌�\\u"閸氬氦绔熼弰顖氭儊閺堝娲撴稉顏勭摟缁楋拷
						boolean isUnicode = true;
						for (int i = 0; i < 4; i++) { // 闁秴宸婚崶娑楅嚋鐎涙顑�
							c = in[off++];
							switch (c) {
							case '0':
							case '1':
							case '2':
							case '3':
							case '4':
							case '5':
							case '6':
							case '7':
							case '8':
							case '9':
								value = (value << 4) + c - '0';
								break;
							case 'a':
							case 'b':
							case 'c':
							case 'd':
							case 'e':
							case 'f':
								value = (value << 4) + 10 + c - 'a';
								break;
							case 'A':
							case 'B':
							case 'C':
							case 'D':
							case 'E':
							case 'F':
								value = (value << 4) + 10 + c - 'A';
								break;
							default:
								isUnicode = false; // 閸掋倖鏌囬弰顖氭儊娑撶皫nicode閻拷
							}
						}
						if (isUnicode) { // 閺勭椃nicode閻浇娴嗛幑顤嶈礋鐎涙顑�
							out[outLen++] = (char) value;
						} else { // 娑撳秵妲竨nicode閻焦濡�\\uXXXX"婵夘偄鍙嗘潻鏂挎礀閸婏拷
							off = off - 4;
							out[outLen++] = '\\';
							out[outLen++] = 'u';
							out[outLen++] = in[off++];
						}
					} else { // 娑撳秴顧愰崶娑楅嚋鐎涙顑侀崚娆愬Ω"\\u"閺�儳鍙嗘潻鏂挎礀缂佹挻鐏夐獮鍓佹埛缂侊拷
						out[outLen++] = '\\';
						out[outLen++] = 'u';
						continue;
					}
				} else {
					switch (c) { // 閸掋倖鏌�\\"閸氬氦绔熼弰顖氭儊閹恒儳澹掑▓濠傜摟缁楋讣绱濋崶鐐舵簠閿涘ab娑擄拷琚惃锟�
					case 't':
						c = '\t';
						out[outLen++] = c;
						break;
					case 'r':
						c = '\r';
						out[outLen++] = c;
						break;
					case 'n':
						c = '\n';
						out[outLen++] = c;
						break;
					case 'f':
						c = '\f';
						out[outLen++] = c;
						break;
					default:
						out[outLen++] = '\\';
						out[outLen++] = c;
						break;
					}
				}
			} else {
				out[outLen++] = (char) c;
			}
		}
		return new String(out, 0, outLen);
	}
}

