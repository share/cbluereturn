package com.until;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import android.util.Log;

public class QHttpClient {	
	
	public QHttpClient() {
	}

	/**
	 * Using GET method.
	 * 
	 * @param url
	 *            The remote URL.
	 * @param queryString
	 *            The query string containing parameters
	 * @return Response string.
	 * @throws Exception
	 */
	public String httpGet(String url, String queryString) throws Exception {
		String responseData = null;

		if (queryString != null && !queryString.equals("")) {
			url += "?" + queryString;
		}			
		//log.info("Context = "+context);
		DefaultHttpClient client = new MyHttpClient();
		HttpGet get = new HttpGet(url);
		// Execute the GET call and obtain the response
		HttpResponse getResponse = client.execute(get);
		HttpEntity responseEntity = getResponse.getEntity();

        //System.out.println("Login form get: " + getResponse.getStatusLine());        
        if (responseEntity != null) {
            //responseEntity.consumeContent();
        	responseData = EntityUtils.toString(responseEntity);        
        } 
        //System.out.println("Initial set of cookies:");
        List<Cookie> cookies = client.getCookieStore().getCookies();
        if (cookies.isEmpty()) {
            //System.out.println("None");
        } else {
            for (int i = 0; i < cookies.size(); i++) {
                //System.out.println("- " + cookies.get(i).toString());
            }
        }
		return responseData;
	}
	/**
	 * Using GET method.
	 * 
	 * @param url
	 *            The remote URL.
	 * @param queryString
	 *            The query string containing parameters
	 * @return Response string.
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public String httpGet(String url, HashMap<String , String> map) throws Exception {
		String responseData = null;
		java.util.Iterator it = map.entrySet().iterator();
		int i = 0;
		while(it.hasNext()){
			java.util.Map.Entry entry = (java.util.Map.Entry)it.next();
			if(i == 0){
				if(url.contains("?")){
					url += "&" + entry.getKey()+"="+entry.getValue();
				}else{
				    url += "?" + entry.getKey()+"="+entry.getValue();
				}
			}else{
				url += "&" + entry.getKey()+"="+entry.getValue();
			}
			i++;
		} 
		Log.i("HDD" , "url=" + url);
		DefaultHttpClient client = new MyHttpClient();
		HttpGet get = new HttpGet(url);
		HttpResponse getResponse = client.execute(get);
		HttpEntity responseEntity = getResponse.getEntity();

        if (responseEntity != null) {
        	responseData = UnicodeDecoder.decode(EntityUtils.toString(responseEntity));
        } 
		return responseData;
	}
	
	/**
	 * Using POST method.
	 * 
	 * @param url
	 *            The remote URL.
	 * @param queryString
	 *            The query string containing parameters
	 * @return Response string.
	 * @throws Exception
	 */
	@SuppressWarnings("finally")
	public String httpPost(String url, List<NameValuePair> nameValuePairs) throws Exception {
		HttpResponse response = null;			
		DefaultHttpClient httpClient = new MyHttpClient();	
		HttpPost httppost = new HttpPost(url);
		httpClient.getParams().setIntParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 15000);//设置超时
		try {	       
			String entityValue = URLEncodedUtils.format(nameValuePairs, HTTP.UTF_8);
			// Do your replacement here in entityValue
			entityValue = entityValue.replace("+", "%20");
			//System.out.println("Entity value = "+entityValue);
			StringEntity entity = new StringEntity(entityValue, HTTP.UTF_8);
			entity.setContentType(URLEncodedUtils.CONTENT_TYPE);
			// And now do your posting of this entity			
			httppost.setEntity(entity);
			httppost.setHeader("Range","bytes="+"");
	        response = httpClient.execute(httppost);
	        
	        Log.i("HDD","URL = " + url);
	        Log.i("HDD","nameValuePairs = " + nameValuePairs);
	        
		} catch (Exception e) {
			response = null;
			e.printStackTrace();
		}finally{
			return httpResponseToString(response);
		}		
//		return DataUtils.httpResponseToString(response);		
	}		
	
	/**
	 * ��ȡ���
	 * @param response
	 * @return
	 */
	public static String httpResponseToString(HttpResponse response) {
		try {
			if (response == null) {
				return null;
			}
//			Log.i("HDD","���󷵻���ݣ�" + response.toString());
			InputStream is = response.getEntity().getContent();
			BufferedReader r = new BufferedReader(new InputStreamReader(is));
			StringBuilder total = new StringBuilder();
			String line;
			while ((line = r.readLine()) != null) {
				total.append(line);
			}
			r.close();
			is.close();
			String result = total.toString();
			return result;
			
//			InputStream is = response.getEntity().getContent();
//			BufferedInputStream bis = new BufferedInputStream(is);
//			ByteArrayBuffer baf = new ByteArrayBuffer(new InputStreamReader(is));
//			int current = 0;
//			while((current = bis.read()) != -1){
//				baf.append((byte)current);
//			}
//			String text = new String(baf.toByteArray(),"utf-8"); //ָ������
//			Log.i("HDD","���󷵻���ݣ�" + text);
//			return text;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
