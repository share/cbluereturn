package com.bean;

/**
 * 一天数据的对象类
 * @author zhangbo
 *
 */
public class DayData {

	private String userid; // 用户设备id
	private int date = -1; // 日期 例：20131213
	private int half_date = -1; // 0：上午； 1：下午
	private int num_plan = -1; // 计划值
	private int num_actual = -1; // 实际值
	private int num_00 = -1;
	private int num_01 = -1;
	private int num_02 = -1;
	private int num_03 = -1;
	private int num_04 = -1;
	private int num_05 = -1;
	private int num_06 = -1;
	private int num_07 = -1;
	private int num_08 = -1;
	private int num_09 = -1;
	private int num_10 = -1;
	private int num_11 = -1;
	private int num_12 = -1;
	private int num_13 = -1;
	private int num_14 = -1;
	private int num_15 = -1;
	private int num_16 = -1;
	private int num_17 = -1;
	private int num_18 = -1;
	private int num_19 = -1;
	private int num_20 = -1;
	private int num_21 = -1;
	private int num_22 = -1;
	private int num_23 = -1;
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public int getDate() {
		return date;
	}
	public void setDate(int date) {
		this.date = date;
	}
	public int getHalf_date() {
		return half_date;
	}
	public void setHalf_date(int half_date) {
		this.half_date = half_date;
	}
	public int getNum_plan() {
		return num_plan;
	}
	public void setNum_plan(int num_plan) {
		this.num_plan = num_plan;
	}
	public int getNum_actual() {
		return num_actual;
	}
	public void setNum_actual(int num_actual) {
		this.num_actual = num_actual;
	}
	public int getNum_00() {
		return num_00;
	}
	public void setNum_00(int num_00) {
		this.num_00 = num_00;
	}
	public int getNum_01() {
		return num_01;
	}
	public void setNum_01(int num_01) {
		this.num_01 = num_01;
	}
	public int getNum_02() {
		return num_02;
	}
	public void setNum_02(int num_02) {
		this.num_02 = num_02;
	}
	public int getNum_03() {
		return num_03;
	}
	public void setNum_03(int num_03) {
		this.num_03 = num_03;
	}
	public int getNum_04() {
		return num_04;
	}
	public void setNum_04(int num_04) {
		this.num_04 = num_04;
	}
	public int getNum_05() {
		return num_05;
	}
	public void setNum_05(int num_05) {
		this.num_05 = num_05;
	}
	public int getNum_06() {
		return num_06;
	}
	public void setNum_06(int num_06) {
		this.num_06 = num_06;
	}
	public int getNum_07() {
		return num_07;
	}
	public void setNum_07(int num_07) {
		this.num_07 = num_07;
	}
	public int getNum_08() {
		return num_08;
	}
	public void setNum_08(int num_08) {
		this.num_08 = num_08;
	}
	public int getNum_09() {
		return num_09;
	}
	public void setNum_09(int num_09) {
		this.num_09 = num_09;
	}
	public int getNum_10() {
		return num_10;
	}
	public void setNum_10(int num_10) {
		this.num_10 = num_10;
	}
	public int getNum_11() {
		return num_11;
	}
	public void setNum_11(int num_11) {
		this.num_11 = num_11;
	}
	public int getNum_12() {
		return num_12;
	}
	public void setNum_12(int num_12) {
		this.num_12 = num_12;
	}
	public int getNum_13() {
		return num_13;
	}
	public void setNum_13(int num_13) {
		this.num_13 = num_13;
	}
	public int getNum_14() {
		return num_14;
	}
	public void setNum_14(int num_14) {
		this.num_14 = num_14;
	}
	public int getNum_15() {
		return num_15;
	}
	public void setNum_15(int num_15) {
		this.num_15 = num_15;
	}
	public int getNum_16() {
		return num_16;
	}
	public void setNum_16(int num_16) {
		this.num_16 = num_16;
	}
	public int getNum_17() {
		return num_17;
	}
	public void setNum_17(int num_17) {
		this.num_17 = num_17;
	}
	public int getNum_18() {
		return num_18;
	}
	public void setNum_18(int num_18) {
		this.num_18 = num_18;
	}
	public int getNum_19() {
		return num_19;
	}
	public void setNum_19(int num_19) {
		this.num_19 = num_19;
	}
	public int getNum_20() {
		return num_20;
	}
	public void setNum_20(int num_20) {
		this.num_20 = num_20;
	}
	public int getNum_21() {
		return num_21;
	}
	public void setNum_21(int num_21) {
		this.num_21 = num_21;
	}
	public int getNum_22() {
		return num_22;
	}
	public void setNum_22(int num_22) {
		this.num_22 = num_22;
	}
	public int getNum_23() {
		return num_23;
	}
	public void setNum_23(int num_23) {
		this.num_23 = num_23;
	}
}
