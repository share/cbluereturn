package com.bean;

/**
 * 用户信息实体类
 * @author zhangbo
 *
 */
public class UserInfo {

	private String name = ""; // 姓名
	private String gender = ""; // 性别
	private String age = ""; // 年龄
	private String flavor = ""; // 口味
	private String nicotine = ""; // 尼古丁
	private String brand = ""; // 品牌
	private String price = ""; // 价格
	private String cnicotine = ""; // 香烟尼古丁含量
	private String cprice = ""; // 香烟价格
	private String cnum = ""; // 香烟每天根数
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getFlavor() {
		return flavor;
	}
	public void setFlavor(String flavor) {
		this.flavor = flavor;
	}
	public String getNicotine() {
		return nicotine;
	}
	public void setNicotine(String nicotine) {
		this.nicotine = nicotine;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getCnicotine() {
		return cnicotine;
	}
	public void setCnicotine(String cnicotine) {
		this.cnicotine = cnicotine;
	}
	public String getCprice() {
		return cprice;
	}
	public void setCprice(String cprice) {
		this.cprice = cprice;
	}
	public String getCnum() {
		return cnum;
	}
	public void setCnum(String cnum) {
		this.cnum = cnum;
	}
	
}
