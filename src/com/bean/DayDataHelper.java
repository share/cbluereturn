package com.bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.until.DateUtil;

public class DayDataHelper {

	/**
	 * 根据byte数组生成对象
	 * @param bytes
	 * @return
	 */
	public static DayData byteToObject(byte[] bytes) {
		DayData dayData = new DayData();
		
		// 获取日期
		int day = (bytes[1] & 0xFF);
		int month = (bytes[2] & 0xFF);
		int year =  (bytes[3] & 0xFF);
		String strYear = "20" + year;
		String strMonth = "" + month;
		String strDay = "" + day;
		if (strMonth.length() == 1) {
			strMonth = "0" + strMonth;
		}
		if (strDay.length() == 1) {
			strDay = "0" + strDay;
		}
		String dateStr = strYear + strMonth + strDay;
		dayData.setDate(Integer.valueOf(dateStr));
		
		// 如果是上半天数据
		if ((bytes[4] & 0xFF) == 0) {
			dayData.setHalf_date((bytes[4] & 0xFF));
			if ((bytes[5] & 0xFF) != 255) {
				dayData.setNum_00((bytes[5] & 0xFF));
			}
			if ((bytes[6] & 0xFF) != 255) {
				dayData.setNum_01((bytes[6] & 0xFF));
			}
			if ((bytes[7] & 0xFF) != 255) {
				dayData.setNum_02((bytes[7] & 0xFF));
			}
			if ((bytes[8] & 0xFF) != 255) {
				dayData.setNum_03((bytes[8] & 0xFF));
			}
			if ((bytes[9] & 0xFF) != 255) {
				dayData.setNum_04((bytes[9] & 0xFF));
			}
			if ((bytes[10] & 0xFF) != 255) {
				dayData.setNum_05((bytes[10] & 0xFF));
			}
			if ((bytes[11] & 0xFF) != 255) {
				dayData.setNum_06((bytes[11] & 0xFF));
			}
			if ((bytes[12] & 0xFF) != 255) {
				dayData.setNum_07((bytes[12] & 0xFF));
			}
			if ((bytes[13] & 0xFF) != 255) {
				dayData.setNum_08((bytes[13] & 0xFF));
			}
			if ((bytes[14] & 0xFF) != 255) {
				dayData.setNum_09((bytes[14] & 0xFF));
			}
			if ((bytes[15] & 0xFF) != 255) {
				dayData.setNum_10((bytes[15] & 0xFF));
			}
			if ((bytes[16] & 0xFF) != 255) {
				dayData.setNum_11((bytes[16] & 0xFF));
			}
		}
		// 如果是下半天数据
		else if ((bytes[4] & 0xFF) == 1) {
			dayData.setHalf_date((bytes[4] & 0xFF));
			if ((bytes[5] & 0xFF) != 255) {
				dayData.setNum_12((bytes[5] & 0xFF));
			}
			if ((bytes[6] & 0xFF) != 255) {
				dayData.setNum_13((bytes[6] & 0xFF));
			}
			if ((bytes[7] & 0xFF) != 255) {
				dayData.setNum_14((bytes[7] & 0xFF));
			}
			if ((bytes[8] & 0xFF) != 255) {
				dayData.setNum_15((bytes[8] & 0xFF));
			}
			if ((bytes[9] & 0xFF) != 255) {
				dayData.setNum_16((bytes[9] & 0xFF));
			}
			if ((bytes[10] & 0xFF) != 255) {
				dayData.setNum_17((bytes[10] & 0xFF));
			}
			if ((bytes[11] & 0xFF) != 255) {
				dayData.setNum_18((bytes[11] & 0xFF));
			}
			if ((bytes[12] & 0xFF) != 255) {
				dayData.setNum_19((bytes[12] & 0xFF));
			}
			if ((bytes[13] & 0xFF) != 255) {
				dayData.setNum_20((bytes[13] & 0xFF));
			}
			if ((bytes[14] & 0xFF) != 255) {
				dayData.setNum_21((bytes[14] & 0xFF));
			}
			if ((bytes[15] & 0xFF) != 255) {
				dayData.setNum_22((bytes[15] & 0xFF));
			}
			if ((bytes[16] & 0xFF) != 255) {
				dayData.setNum_23((bytes[16] & 0xFF));
			}
		}
		
		return dayData;
	}
	
	/**
	 * 根据jsonObj生成对象
	 * @param obj
	 * @return
	 */
	public static DayData fomateJsonToObject(JSONObject obj) {
		DayData dayData = new DayData();
		
		try {
			dayData.setDate(Integer.valueOf(obj.getString("date")));
			dayData.setNum_plan(Integer.valueOf(obj.getString("num_plan")));
			dayData.setNum_actual(Integer.valueOf(obj.getString("num_actual")));
			dayData.setNum_00(Integer.valueOf(obj.getString("num_00")));
			dayData.setNum_01(Integer.valueOf(obj.getString("num_01")));
			dayData.setNum_02(Integer.valueOf(obj.getString("num_02")));
			dayData.setNum_03(Integer.valueOf(obj.getString("num_03")));
			dayData.setNum_04(Integer.valueOf(obj.getString("num_04")));
			dayData.setNum_05(Integer.valueOf(obj.getString("num_05")));
			dayData.setNum_06(Integer.valueOf(obj.getString("num_06")));
			dayData.setNum_07(Integer.valueOf(obj.getString("num_07")));
			dayData.setNum_08(Integer.valueOf(obj.getString("num_08")));
			dayData.setNum_09(Integer.valueOf(obj.getString("num_09")));
			dayData.setNum_10(Integer.valueOf(obj.getString("num_10")));
			dayData.setNum_11(Integer.valueOf(obj.getString("num_11")));
			dayData.setNum_12(Integer.valueOf(obj.getString("num_12")));
			dayData.setNum_13(Integer.valueOf(obj.getString("num_13")));
			dayData.setNum_14(Integer.valueOf(obj.getString("num_14")));
			dayData.setNum_15(Integer.valueOf(obj.getString("num_15")));
			dayData.setNum_16(Integer.valueOf(obj.getString("num_16")));
			dayData.setNum_17(Integer.valueOf(obj.getString("num_17")));
			dayData.setNum_18(Integer.valueOf(obj.getString("num_18")));
			dayData.setNum_19(Integer.valueOf(obj.getString("num_19")));
			dayData.setNum_20(Integer.valueOf(obj.getString("num_20")));
			dayData.setNum_21(Integer.valueOf(obj.getString("num_21")));
			dayData.setNum_22(Integer.valueOf(obj.getString("num_22")));
			dayData.setNum_23(Integer.valueOf(obj.getString("num_23")));
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return dayData;
	}
	
	/**
	 * 将list转换成jsonArray
	 * @param list
	 * @return
	 */
	public static JSONArray fomateListToJsonArray(List<DayData> list) {
		JSONArray jsonArr = new JSONArray();
		
		try {
			for (DayData dayData : list) {
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("date", String.valueOf(dayData.getDate()));
				jsonObj.put("num_plan", String.valueOf(dayData.getNum_plan()));
				jsonObj.put("num_actual", String.valueOf(dayData.getNum_actual()));
				jsonObj.put("num_00", String.valueOf(dayData.getNum_00()));
				jsonObj.put("num_01", String.valueOf(dayData.getNum_01()));
				jsonObj.put("num_02", String.valueOf(dayData.getNum_02()));
				jsonObj.put("num_03", String.valueOf(dayData.getNum_03()));
				jsonObj.put("num_04", String.valueOf(dayData.getNum_04()));
				jsonObj.put("num_05", String.valueOf(dayData.getNum_05()));
				jsonObj.put("num_06", String.valueOf(dayData.getNum_06()));
				jsonObj.put("num_07", String.valueOf(dayData.getNum_07()));
				jsonObj.put("num_08", String.valueOf(dayData.getNum_08()));
				jsonObj.put("num_09", String.valueOf(dayData.getNum_09()));
				jsonObj.put("num_10", String.valueOf(dayData.getNum_10()));
				jsonObj.put("num_11", String.valueOf(dayData.getNum_11()));
				jsonObj.put("num_12", String.valueOf(dayData.getNum_12()));
				jsonObj.put("num_13", String.valueOf(dayData.getNum_13()));
				jsonObj.put("num_14", String.valueOf(dayData.getNum_14()));
				jsonObj.put("num_15", String.valueOf(dayData.getNum_15()));
				jsonObj.put("num_16", String.valueOf(dayData.getNum_16()));
				jsonObj.put("num_17", String.valueOf(dayData.getNum_17()));
				jsonObj.put("num_18", String.valueOf(dayData.getNum_18()));
				jsonObj.put("num_19", String.valueOf(dayData.getNum_19()));
				jsonObj.put("num_20", String.valueOf(dayData.getNum_20()));
				jsonObj.put("num_21", String.valueOf(dayData.getNum_21()));
				jsonObj.put("num_22", String.valueOf(dayData.getNum_22()));
				jsonObj.put("num_23", String.valueOf(dayData.getNum_23()));
				
				jsonArr.put(jsonObj);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return jsonArr;
	}
	
	/**
	 * 转换成double数组
	 * @param dayData
	 * @return
	 */
	public static double[] getDayDataDoubleArray(DayData dayData) {
		double[] tempdouble = new double[24];
		tempdouble[0] = dayData.getNum_00();
		tempdouble[1] = dayData.getNum_01();
		tempdouble[2] = dayData.getNum_02();
		tempdouble[3] = dayData.getNum_03();
		tempdouble[4] = dayData.getNum_04();
		tempdouble[5] = dayData.getNum_05();
		tempdouble[6] = dayData.getNum_06();
		tempdouble[7] = dayData.getNum_07();
		tempdouble[8] = dayData.getNum_08();
		tempdouble[9] = dayData.getNum_09();
		tempdouble[10] = dayData.getNum_10();
		tempdouble[11] = dayData.getNum_11();
		tempdouble[12] = dayData.getNum_12();
		tempdouble[13] = dayData.getNum_13();
		tempdouble[14] = dayData.getNum_14();
		tempdouble[15] = dayData.getNum_15();
		tempdouble[16] = dayData.getNum_16();
		tempdouble[17] = dayData.getNum_17();
		tempdouble[18] = dayData.getNum_18();
		tempdouble[19] = dayData.getNum_19();
		tempdouble[20] = dayData.getNum_20();
		tempdouble[21] = dayData.getNum_21();
		tempdouble[22] = dayData.getNum_22();
		tempdouble[23] = dayData.getNum_23();
		
		return tempdouble;
	}
	
	/**
	 * 获取当天显示的数据
	 * @param dayData
	 * @return
	 */
	public static double[] makeFirstDayDoubleArray(DayData dayData) {
		double[] tempdouble = new double[24];
		tempdouble[0] = dayData.getNum_00();
		tempdouble[1] = dayData.getNum_01();
		tempdouble[2] = dayData.getNum_02();
		tempdouble[3] = dayData.getNum_03();
		tempdouble[4] = dayData.getNum_04();
		tempdouble[5] = dayData.getNum_05();
		tempdouble[6] = dayData.getNum_06();
		tempdouble[7] = dayData.getNum_07();
		tempdouble[8] = dayData.getNum_08();
		tempdouble[9] = dayData.getNum_09();
		tempdouble[10] = dayData.getNum_10();
		tempdouble[11] = dayData.getNum_11();
		tempdouble[12] = dayData.getNum_12();
		tempdouble[13] = dayData.getNum_13();
		tempdouble[14] = dayData.getNum_14();
		tempdouble[15] = dayData.getNum_15();
		tempdouble[16] = dayData.getNum_16();
		tempdouble[17] = dayData.getNum_17();
		tempdouble[18] = dayData.getNum_18();
		tempdouble[19] = dayData.getNum_19();
		tempdouble[20] = dayData.getNum_20();
		tempdouble[21] = dayData.getNum_21();
		tempdouble[22] = dayData.getNum_22();
		tempdouble[23] = dayData.getNum_23();
		
		int dataLength = 0;
		for (int i = tempdouble.length - 1; i >= 0; i--) {
			if (tempdouble[i] >= 0) {
				dataLength = i + 1;
				break;
			} 
		}
		
		double[] newDoubles = new double[dataLength];
		for (int i = 0; i < newDoubles.length; i++) {
			newDoubles[i] = tempdouble[i];
		}
		
		return newDoubles;
	}
	
	/**
	 * 获取当前周的实际值和计划值
	 * @param dataList
	 * @param thisMondayStr
	 * @return
	 */
	public static Map<String, double[]> getWeekDataDoubleArray(List<DayData> dataList, String thisMondayStr) {
		Map<String, double[]> map = new HashMap<String, double[]>();
		
		double actualTotalNum = 0; // 计算总实际值
		double[] actualTotalNumArray = new double[1];
		double[] actualDouble = new double[7];
		double[] planDouble = new double[7];
		for (DayData dayData : dataList) {
			for (int i = 0; i < 7; i++) {
				if (Integer.valueOf(DateUtil.getDifferDate(thisMondayStr, i)) == dayData.getDate()) {
					actualDouble[i] = dayData.getNum_actual();
					planDouble[i] = dayData.getNum_plan();
					
					actualTotalNum += dayData.getNum_actual(); // 计算总实际值
					break;
				}
			}
		}
		
		map.put("actualDouble", actualDouble);
		map.put("planDouble", planDouble);
		actualTotalNumArray[0] = actualTotalNum;
		map.put("actualTotalNumArray", actualTotalNumArray);
		
		return map;
	}
	
	/**
	 * 获取当前月的实际值和计划值
	 * @param dataList
	 * @param thisMonthStr
	 * @return
	 */
	public static Map<String, double[]> getMonthActualDataDoubleArray(List<DayData> dataList, String thisMonthStr) {
		Map<String, double[]> map = new HashMap<String, double[]>();
		
		double actualTotalNum = 0; // 计算总实际值
		double[] actualTotalNumArray = new double[1];
		double[] actualDouble = new double[31];
		double[] planDouble = new double[31];
		for (DayData dayData : dataList) {
			String monthStr = String.valueOf(dayData.getDate()).substring(0, 6);
			int day = Integer.valueOf(String.valueOf(dayData.getDate()).substring(6, 8));
			if (thisMonthStr.equals(monthStr)) {
				actualDouble[day - 1] = dayData.getNum_actual();
				planDouble[day - 1] = dayData.getNum_plan();
				
				actualTotalNum += dayData.getNum_actual(); // 计算总实际值
			}
		}
		
		map.put("actualDouble", actualDouble);
		map.put("planDouble", planDouble);
		actualTotalNumArray[0] = actualTotalNum;
		map.put("actualTotalNumArray", actualTotalNumArray);
		
		return map;
	}
	
	/**
	 * 获取当前年的实际值和计划值
	 * @param dataList
	 * @param thisYearStr
	 * @return
	 */
	public static Map<String, double[]> getYearActualDataDoubleArray(List<DayData> dataList, String thisYearStr) {
		Map<String, double[]> map = new HashMap<String, double[]>();
		
		double actualTotalNum = 0; // 计算总实际值
		double[] actualTotalNumArray = new double[1];
		double[] actualDoubleOfYear = new double[31 * 12];
		double[] planDoubleOfYear = new double[31 * 12];
		
		for (int i = 0; i < 12; i++) {
			String strMonth = "" + (i + 1);
			if (strMonth.length() == 1) {
				strMonth = "0" + strMonth;
			}
			String thisMonthStr = thisYearStr + strMonth;
			
			Map<String, double[]> monthMap = getMonthActualDataDoubleArray(dataList, thisMonthStr);
			double[] actualDouble = monthMap.get("actualDouble");
			double[] planDouble = monthMap.get("planDouble");
			double[] actualTotalNumArrayOfMonth = monthMap.get("actualTotalNumArray");
			actualTotalNum += actualTotalNumArrayOfMonth[0];
			
			for (int j = 0; j < 31; j++) {
				int thisPosition = i * 31 + j;
				actualDoubleOfYear[thisPosition] = actualDouble[j];
				planDoubleOfYear[thisPosition] = planDouble[j];
			}
		}
		
		map.put("actualDouble", actualDoubleOfYear);
		map.put("planDouble", planDoubleOfYear);
		actualTotalNumArray[0] = actualTotalNum;
		map.put("actualTotalNumArray", actualTotalNumArray);
		
		return map;
	}
	
	/**
	 * 获取年的x轴数据
	 * @return
	 */
	public static double[] getYearXAxisNums() {
	
		double consNum = 0.032258;
		double[] xAxisNumsdouble = new double[31 * 12];
		
		for (int i = 0; i < 12; i++) {
			double monthDouble = (double)i;
			
			for (int j = 0; j < 31; j++) {
				int thisPosition = i * 31 + j;
				xAxisNumsdouble[thisPosition] = monthDouble + 1 + consNum * j;
			}
		}
		
		return xAxisNumsdouble;
	}
}
